package com.e.gameroom;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

import java.util.ArrayList;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

class ScoreArrayAdapter extends ArrayAdapter<String> {

    private static final String TAG_LOG = ScoreArrayAdapter.class.getName();

    private final Context mContext;
    private final ArrayList<String> mUsernames;
    private final ArrayList<Long> mScores;
    private int mResource;

    //Interface Elements
    private TextView mPosition, mUsername, mScore;

    public ScoreArrayAdapter(@NonNull Context context, int resource,
                             @NonNull ArrayList<String> usernames,
                             @NonNull ArrayList<Long> scores) {

        super(context, resource, usernames);
        this.mResource = resource;
        this.mContext = context;
        this.mUsernames = usernames;
        this.mScores = scores;
    }

    //Function that creates the view of the single Score using a layout.xml and set the values in the various graphic objects
    @NonNull
    @Override
    public View getView(int pos, @Nullable View convertView, @NonNull ViewGroup parent) {
        LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(android.content.Context.LAYOUT_INFLATER_SERVICE);

        convertView = inflater.inflate(mResource, parent, false);

        //Elements Assignment
        mPosition = convertView.findViewById(R.id.txtPosition);
        mUsername = convertView.findViewById(R.id.txtUsername);
        mScore = convertView.findViewById(R.id.txtScoreData);

        //Set elements value
        mPosition.setText(String.valueOf(pos + 1));
        mUsername.setText(mUsernames.get(pos));
        mScore.setText(String.valueOf(mScores.get(pos)));

        Log.d(TAG_LOG, "Username: " + mUsername.getText() + "  Score: " + mScore.getText());

        //set Current highScore
        highScoreCurrentUser(mPosition, mUsername, mScore, mUsername.getText().toString());

        return convertView;
    }

    //Variable that personalizes the user's score if exists
    private void highScoreCurrentUser(TextView Position, TextView Username, TextView Score, String username) {
        FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
        if (user != null) {
            Log.d("User Exist", user.getDisplayName());
            String UsernameString = user.getDisplayName();
            if (UsernameString.equals(username)) {

                //set personal score Elements Color
                Position.setTextColor(getContext().getResources().getColor(R.color.green_verify));
                Username.setTextColor(getContext().getResources().getColor(R.color.green_verify));
                Score.setTextColor(getContext().getResources().getColor(R.color.green_verify));

            }
        } else {
            Log.d(TAG_LOG, "There are no scores for the current user in this game");
        }
    }

    @Override
    public void notifyDataSetChanged() {
        super.notifyDataSetChanged();
    }

}
