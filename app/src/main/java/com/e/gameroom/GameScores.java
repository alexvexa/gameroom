package com.e.gameroom;

import android.content.Context;
import android.database.Cursor;
import android.widget.Toast;

import com.e.gameroom.database.DatabaseHelper;

import java.util.ArrayList;

public class GameScores {
    String gameName;
    ArrayList<StringBuffer> scores;

    public GameScores(String GameName, Context context) {
        this.gameName = GameName;
        this.scores = getGameScores(GameName, context);
    }

    public ArrayList<StringBuffer> getScore() {
        return scores;
    }



    //Function that returns the scores of the selected game
    public ArrayList<StringBuffer> getGameScores(String gameName, Context context) {
        ArrayList<StringBuffer> gameScores = new ArrayList<>();

        //Db Reference
        DatabaseHelper myDb;
        myDb = new DatabaseHelper(context);

        //Execute Query for get All Scores
        Cursor res = myDb.getAllData();

        if (res.getCount() == 0) {
            //ShowMessage no Results
            Toast.makeText(context, "Error, data not founded", Toast.LENGTH_SHORT).show();
        } else {

            gameScores = new ArrayList<StringBuffer>();
            while (res.moveToNext()) {
                if (res.getString(2).equals(gameName)) {
                    //Create item Score String and put on gameScores List
                    StringBuffer TemporaryBuffer = new StringBuffer();
                    TemporaryBuffer.append("\t" + res.getString(1) + "\t\t\tScore: " + res.getString(3));
                    gameScores.add(TemporaryBuffer);
                }
            }
        }
        return gameScores;
    }
}
