package com.e.gameroom;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;

import com.e.gameroom.GameScores;
import com.e.gameroom.R;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.StringRes;
import androidx.viewpager.widget.PagerAdapter;

public class ScoresAdapter extends PagerAdapter {
    private List<GameScores> scoreList;
    private LayoutInflater layout;
    private Context context;

    //Game Title List for tabLayout
    @StringRes
    private static final int[] TAB_TITLES = new int[]{
            R.string.catch_the_ball_Name,
            R.string.Snake_Game_Name,
            R.string.Breakout_Game_Name,
            R.string.SpaceInvaders_Game_Name};


    public ScoresAdapter(List<GameScores> ScoresList, Context applicationContext) {
        this.scoreList = ScoresList;
        this.context = applicationContext;
    }

    public int getCount() {
        return scoreList.size();
    }

    @Override
    public boolean isViewFromObject(@NonNull View view, @NonNull Object object) {
        return view.equals(object);
    }

    @NonNull
    @Override
    public Object instantiateItem(@NonNull ViewGroup container, final int position) {
        //inflate Layout
        layout = LayoutInflater.from(context);
        View view = layout.inflate(R.layout.scorelist_layout, container, false);

        //ListView Assignment for setAdapter
        ListView listView = view.findViewById(R.id.Local_list);
        ArrayAdapter<StringBuffer> bufferAdapter = new ArrayAdapter<StringBuffer>(context, R.layout.guest_score_item_layout, scoreList.get(position).getScore());
        listView.setAdapter(bufferAdapter);
        container.addView(view, 0);

        return view;
    }


    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((View) object);
    }


    @Nullable
    @Override
    public CharSequence getPageTitle(int position) {
        return context.getResources().getString(TAB_TITLES[position]);
    }
}

