package com.e.gameroom;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.os.SystemClock;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.widget.ImageView;
import java.lang.ref.WeakReference;


public class SplashartActivity extends Activity {

    private static final String TAG_LOG = SplashartActivity.class.getName();
    private static final long MIN_WAIT_INTERVAL = 1500L;
    private static final long MAX_WAIT_INTERVAL = 3000L;
    private static final int GO_AHEAD_WHAT = 1;
    private static final String IS_DONE_KEY = "com.e.gameroom.key.IS_DONE_KEY";
    private static final String START_TIME_KEY = "com.e.gameroom.key.START_TIME_KEY";


    private long mStartTime = 0L;
    private boolean mIsDone;
    private UiHandler mHandler;

    //Interface Elements Reference (Layout)
    private ImageView mImage;

    //Status sent Class
    private static class UiHandler extends Handler {
        private WeakReference<SplashartActivity> mActivityRef;

        public UiHandler(final SplashartActivity srcActivity) {
            this.mActivityRef = new WeakReference<>(srcActivity);
        }

        @Override
        public void handleMessage(Message msg) {
            Log.d(TAG_LOG, "SPLASH-ACTIVITY: ricezione messaggio");
            final SplashartActivity srcActivity = this.mActivityRef.get();
            if (srcActivity == null) {
                Log.d(TAG_LOG, "Reference to StateSplashActivity lost!");
                return;
            }

            switch (msg.what) {
                case GO_AHEAD_WHAT:
                    long elapsedTime = SystemClock.uptimeMillis() - srcActivity.mStartTime;
                        if (elapsedTime >= MIN_WAIT_INTERVAL && !srcActivity.mIsDone) {
                            srcActivity.mIsDone = true;
                            srcActivity.goAhead();
                    }
                    break;
            }
        }
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putBoolean(IS_DONE_KEY, mIsDone);
        outState.putLong(START_TIME_KEY, mStartTime);
    }

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash_art);
        View decorView = getWindow().getDecorView();
        int uiOptions = View.SYSTEM_UI_FLAG_HIDE_NAVIGATION;
        decorView.setSystemUiVisibility(uiOptions);

        if (savedInstanceState != null) {
            Log.d(TAG_LOG, "SPLASH-ACTIVITY: ripristino timer dopo switch");
            this.mStartTime = savedInstanceState.getLong(START_TIME_KEY);
        }
        mHandler = new UiHandler(this);
        mImage = findViewById(R.id.Background);
        mImage.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                Log.d(TAG_LOG, "SPLASH-ACTIVITY: click immagine");

                long elaspedTime = SystemClock.uptimeMillis() - mStartTime;
                if (elaspedTime >= MIN_WAIT_INTERVAL && !mIsDone) {
                    mIsDone = true;
                    goAhead();
                } else {
                    Log.d(TAG_LOG, "Too early!");
                }
                return false;
            }
        });
    }

    @Override
    protected void onStart() {
        super.onStart();

        //Start Timer
        if (mStartTime == 0L) {
            mStartTime = SystemClock.uptimeMillis();
        }

        // We set the time for going ahead automatically
        final Message goAheadMessage = mHandler.obtainMessage(GO_AHEAD_WHAT);
        mHandler.sendMessageAtTime(goAheadMessage, mStartTime + MAX_WAIT_INTERVAL);
        Log.d(TAG_LOG, "Handler message sent!");
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        //Restore mIsDone Value
        this.mIsDone = savedInstanceState.getBoolean(IS_DONE_KEY);
    }


    //Start next Activity method
    private void goAhead() {
        final Intent intent = new Intent(this, LoginActivity.class);
        startActivity(intent);
        finish();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mHandler.removeCallbacksAndMessages(null);
    }
}
