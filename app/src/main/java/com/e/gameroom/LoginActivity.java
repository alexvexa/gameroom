package com.e.gameroom;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Paint;
import android.graphics.Typeface;
import android.os.Build;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.textfield.TextInputLayout;
import com.google.firebase.auth.AuthCredential;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.EmailAuthProvider;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.GoogleAuthProvider;
import com.google.firebase.database.FirebaseDatabase;

import java.util.HashMap;
import java.util.Map;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

public class LoginActivity extends AppCompatActivity {
    private static final String TAG_LOG = LoginActivity.class.getName();
    private static final int PASSWORD_LENGTH = 6;
    private static final String EMAIL_AT= "@";

    //Google Login Request_Code
    private final static int RC_SIGN_IN = 100;

    //Firebase database persistence enabling
    static {
        FirebaseDatabase.getInstance().setPersistenceEnabled(true);
        Log.d(TAG_LOG,"Firebase Persistence activated");
    }

    //Firebase Connect
    private FirebaseAuth mAuth;
    private FirebaseDatabase mDatabase;
    private FirebaseUser mCurrentUser;
    private GoogleSignInClient mGoogleSignInClient;

    //Interface Elements Reference (Layout)
    private EditText mEmail, mPassword;
    private Button mLoginBtn, mGuestBtn, mGoogleBtn;
    private ProgressBar mProgressBar;
    private TextView mRegisterLink, mResetPasswordLink;
    private TextInputLayout mEmailLayout, mPasswordLayout;

    @Override
    protected void onStart() {
        super.onStart();
        //Auto-Authentication
        if (mAuth.getCurrentUser() != null) {
            Log.d(TAG_LOG, getResources().getString(R.string.Auto_Authentication));
            startActivity(new Intent(getApplicationContext(), HomeActivity.class));
            finish();
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);


        createRequest();

        //Elements assignment
        mResetPasswordLink = findViewById(R.id.forgotPassword);
        mRegisterLink = findViewById(R.id.goToRegister);
        mEmail = findViewById(R.id.email_login);
        mPassword = findViewById(R.id.password_login);
        mLoginBtn = findViewById(R.id.Login);
        mProgressBar = findViewById(R.id.progressBarLogin);
        mGuestBtn = findViewById(R.id.guestBtn);
        mGoogleBtn = findViewById(R.id.GoogleLogin);
        mEmailLayout = findViewById(R.id.EmailLayout);
        mPasswordLayout = findViewById(R.id.PasswordLayout);

        // Firebase initialization
        mAuth = FirebaseAuth.getInstance();

        //TextViews Underline
        mResetPasswordLink.setPaintFlags(Paint.UNDERLINE_TEXT_FLAG);
        mRegisterLink.setPaintFlags(Paint.UNDERLINE_TEXT_FLAG);

        //Guest Btn Action on Click
        mGuestBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getApplicationContext(), HomeActivity.class));
                finish();
            }
        });

        //Register Link Action on Click
        mRegisterLink.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getApplicationContext(), RegisterActivity.class));
            }
        });

        //Google login Btn Action on Click
        mGoogleBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mProgressBar.setVisibility(View.VISIBLE);
                GoogleSignIn();
            }
        });

        //Login Btn Action on Click
        mLoginBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                //Clean layouts Error
                mEmailLayout.setError(null);
                mPasswordLayout.setError(null);

                String email = mEmail.getText().toString().trim();
                String password = mPassword.getText().toString().trim();

                //Email and Password check
                if (TextUtils.isEmpty(email)) {
                    mEmailLayout.setError(getResources().getString(R.string.Email_Required));
                    return;
                } else {
                    if (!email.contains(EMAIL_AT)) {
                        mEmailLayout.setError(getResources().getString(R.string.Wrong_Format));
                        return;

                    }
                }
                if (TextUtils.isEmpty(password)) {
                    mPasswordLayout.setError(getResources().getString(R.string.Password_Required));
                    return;
                }

                if (password.length() < PASSWORD_LENGTH) {
                    mPasswordLayout.setError(getResources().getString(R.string.Password_Length));
                    return;
                }


                //Keyboard input closing
                InputMethodManager inputManager = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
                inputManager.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);




                //ProgressBar set Visibility = VISIBLE
                mProgressBar.setVisibility(View.VISIBLE);

                if (!(mPassword.getText().toString().isEmpty()) && !(mEmail.getText().toString().isEmpty())) {

                    AuthCredential credential = EmailAuthProvider.getCredential(email, password);

                    mAuth.signInWithCredential(credential).addOnCompleteListener(new OnCompleteListener<AuthResult>() {
                        @Override
                        public void onComplete(Task<AuthResult> task) {

                            if (task.isSuccessful()) {
                                //Authentication Successful --> go to HomeActivity
                                Log.d(TAG_LOG, getResources().getString(R.string.Authentication_Success));

                                Intent homeIntent = new Intent(getApplicationContext(), HomeActivity.class);
                                Toast.makeText(LoginActivity.this, R.string.Authentication_Success, Toast.LENGTH_SHORT).show();
                                startActivity(homeIntent);

                                //ProgressBar set Visibility = INVISIBLE
                                mProgressBar.setVisibility(View.INVISIBLE);
                                finish();
                            } else {

                                //Authentication Failed
                                Log.d(TAG_LOG, getResources().getString(R.string.Authentication_Fail), task.getException());
                                Toast.makeText(LoginActivity.this, R.string.Authentication_Fail, Toast.LENGTH_SHORT).show();

                                //ProgressBar set Visibility = INVISIBLE
                                mProgressBar.setVisibility(View.INVISIBLE);

                            }
                        }
                    });
                } else {

                    //ProgressBar set Visibility = INVISIBLE
                    mProgressBar.setVisibility(View.INVISIBLE);
                }
            }
        });


        //Reset Password Link Action on Click
        mResetPasswordLink.setOnClickListener(new View.OnClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.O)
            @Override
            public void onClick(final View v) {

                final EditText resetEmail = new EditText(v.getContext());
                resetEmail.setHint(R.string.Email_Text);
                Typeface tf = getResources().getFont(R.font.arial);
                resetEmail.setTypeface(tf);

                //AlertDialog Builder
                final AlertDialog.Builder passwordResetDialog = new AlertDialog.Builder(v.getContext());

                //AlertDialog construction
                passwordResetDialog.setTitle(R.string.Reset_Text);
                passwordResetDialog.setMessage(R.string.Reset_Insert_Email);
                passwordResetDialog.setView(resetEmail);

                //Confirmation Action on Click
                passwordResetDialog.setPositiveButton(R.string.Confirmation, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                        //extract the email and sent reset link
                        String email = resetEmail.getText().toString();
                        if (!email.isEmpty()) {
                            mAuth.sendPasswordResetEmail(email).addOnSuccessListener(new OnSuccessListener<Void>() {
                                @Override
                                public void onSuccess(Void aVoid) {
                                    Log.w(TAG_LOG, "Reset Email Sent");
                                    Toast.makeText(LoginActivity.this, R.string.Toast_Successful, Toast.LENGTH_SHORT).show();

                                }
                            }).addOnFailureListener(new OnFailureListener() {
                                @Override
                                public void onFailure(@NonNull Exception e) {
                                    Log.w(TAG_LOG, "Reset Email not sent, Error:\n" + e.getMessage());
                                    Toast.makeText(LoginActivity.this, getResources().getString(R.string.Toast_Error) + "\n" + e.getMessage(), Toast.LENGTH_SHORT).show();
                                }
                            });
                        } else {
                            Toast.makeText(LoginActivity.this, R.string.Toast_Email_Empty, Toast.LENGTH_SHORT).show();
                        }
                    }

                });

                //Abort Action on CLick
                passwordResetDialog.setNegativeButton(R.string.Cancel, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        Toast.makeText(LoginActivity.this, R.string.Toast_Abort, Toast.LENGTH_SHORT).show();
                    }
                });

                //show AlertDialog
                passwordResetDialog.create().show();
            }
        });
    }

    //Google Sign In Configuration
    private void createRequest() {
        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestIdToken(getString(R.string.default_web_client_id))
                .requestEmail()
                .build();

        // Build a GoogleSignInClient with the options specified by gso.
        mGoogleSignInClient = GoogleSignIn.getClient(this, gso);
    }

    //Google Login Method
    private void GoogleSignIn() {
        Intent signInIntent = mGoogleSignInClient.getSignInIntent();
        startActivityForResult(signInIntent, RC_SIGN_IN);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        // Result returned from launching the Intent from GoogleSignInApi.getSignInIntent(...);
        if (requestCode == RC_SIGN_IN) {
            Task<GoogleSignInAccount> task = GoogleSignIn.getSignedInAccountFromIntent(data);
            try {
                // Google Sign In was successful, authenticate with Firebase
                GoogleSignInAccount account = task.getResult(ApiException.class);
                Log.d(TAG_LOG, "firebaseAuthWithGoogle:" + account.getId());
                firebaseAuthWithGoogle(account.getIdToken());
            } catch (ApiException e) {
                // Google Sign In failed, update UI appropriately
                Log.w(TAG_LOG, "Google sign in failed", e);
                Toast.makeText(getApplicationContext(), "Google sign in failed" + e.getMessage(), Toast.LENGTH_SHORT).show();

            }
        }
    }

    //Firebase Authentication with Google Account
    private void firebaseAuthWithGoogle(String idToken) {
        final AuthCredential credential = GoogleAuthProvider.getCredential(idToken, null);
        mAuth.signInWithCredential(credential)
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {
                            Log.d(TAG_LOG, getResources().getString(R.string.Authentication_Success));

                            mDatabase = FirebaseDatabase.getInstance();
                            mCurrentUser = mAuth.getCurrentUser();

                            Toast.makeText(LoginActivity.this, R.string.Authentication_Success_Google, Toast.LENGTH_SHORT).show();

                            //Upload Profile Account Info on Firebase
                            Map<String, Object> profileData = new HashMap<>();
                            profileData.put("Username", mCurrentUser.getDisplayName());
                            profileData.put("Email", mCurrentUser.getEmail());
                            String path = "users/" + mCurrentUser.getUid() + "/Profile";
                            mDatabase.getReference(path).updateChildren(profileData);

                            //Authentication Success --> Go to HomeActivity
                            Intent homeIntent = new Intent(getApplicationContext(), HomeActivity.class);
                            startActivity(homeIntent);

                            //ProgressBar set Visibility = INVISIBLE
                            mProgressBar.setVisibility(View.INVISIBLE);
                            finish();
                        } else {

                            // If sign in fails, display a message to the user.
                            Log.w(TAG_LOG, "signInWithCredential:failure", task.getException());
                            Toast.makeText(getApplicationContext(), "Authentication Failed.", Toast.LENGTH_SHORT).show();

                            //ProgressBar set Visibility = INVISIBLE
                            mProgressBar.setVisibility(View.INVISIBLE);
                        }
                    }
                });
    }
}