package com.e.gameroom;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.viewpager.widget.PagerAdapter;

public class GameAdapter extends PagerAdapter {
    private List<Games> mGamelist;
    private LayoutInflater mLayout;
    private Context mContext;

    public GameAdapter(List<Games> gameList, Context applicationContext) {
        this.mGamelist = gameList;
        this.mContext = applicationContext;
    }

    @Override
    public int getCount() {
        return mGamelist.size();
    }

    @Override
    public boolean isViewFromObject(@NonNull View view, @NonNull Object object) {
        return view.equals(object);
    }

    //Function that instantiates the pages of the ViewPager using a Layout.xml and setting the values inside it
    @NonNull
    @Override
    public Object instantiateItem(@NonNull ViewGroup container, final int position) {
        //Inflate layout
        mLayout = LayoutInflater.from(mContext);
        View view = mLayout.inflate(R.layout.gameview_layout, container, false);

        //Inflated Layout Interface Elements Assignment
        ImageView gameIcon;
        gameIcon = view.findViewById(R.id.gameIcon);
        gameIcon.setImageResource(mGamelist.get(position).getImage());
        container.addView(view, 0);

        return view;
    }


    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((View) object);
    }

    public String getPositionTitle(int position) {
        return mGamelist.get(position).getTitle();
    }
}
