package com.e.gameroom;

import android.Manifest;
import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.Typeface;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;
import android.text.InputType;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthCredential;
import com.google.firebase.auth.EmailAuthProvider;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.UserProfileChangeRequest;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.EventListener;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.FirebaseFirestoreException;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;
import com.squareup.picasso.Picasso;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import javax.annotation.Nullable;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import de.hdodenhof.circleimageview.CircleImageView;

public class AccountActivity extends AppCompatActivity {
    private static final String TAG_LOG = AccountActivity.class.getName();
    private static final String IMAGE_MIME_TYPE = "image/*";
    private static final int PASSWORD_LENGTH = 6;
    private static final String AVATAR_FILE_NAME = "avatar.jpg";
    private static final int GALLERY_PERMISSION_REQUEST_CODE = 100;
    private static final int CAMERA_REQUEST_CODE = 2;
    private static final int READ_PERMISSION_REQUEST_CODE = 3;

    //Firebase connect
    private FirebaseAuth mAuth;
    private FirebaseFirestore mStore;
    private FirebaseStorage mStorage;
    private DatabaseReference mDatabaseRef;
    private FirebaseUser mCurrentUser;

    //Interface Elements
    private Button mDeleteBtn, mLogoutBtn;
    private ImageView mAvatarEdit, mBackArrow, mPasswordEdit, mEmailEdit, mUsernameEdit;
    private Drawable mStandardAvatar;
    private TextView mUsername, mEmail;
    private CircleImageView mProfilePic;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_account);

        //Element Assignment
        mBackArrow = findViewById(R.id.BackButton);
        mDeleteBtn = findViewById(R.id.deleteBtn);
        mStandardAvatar = getResources().getDrawable(R.drawable.defaultavatar);
        mAvatarEdit = findViewById(R.id.Avatar_Edit);
        mProfilePic = findViewById(R.id.profile_Image);
        mUsername = findViewById(R.id.username_account);
        mEmail = findViewById(R.id.email_account);
        mPasswordEdit = findViewById(R.id.PasswordEdit);
        mEmailEdit = findViewById(R.id.EmailEdit);
        mUsernameEdit = findViewById(R.id.UsernameEdit);
        mLogoutBtn = findViewById(R.id.logoutBtn);

        //Firebase Instanziation
        mAuth = FirebaseAuth.getInstance();
        mStore = FirebaseFirestore.getInstance();
        mStorage = FirebaseStorage.getInstance();
        mCurrentUser = mAuth.getCurrentUser();
        mDatabaseRef = FirebaseDatabase.getInstance().getReference("/users");

        //Load user Data on View
        setUserInfo();

        //Delete Btn Action on Click
        mDeleteBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                //Build Alert Dialog
                final AlertDialog.Builder deleteAccountDialog = new AlertDialog.Builder(v.getContext());
                deleteAccountDialog.setTitle(R.string.Delete_Account);

                //Confirmation Selection
                deleteAccountDialog.setPositiveButton(R.string.Confirmation, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                        //Delete all Account info (Avatar - Scores - Account Registration)
                        mDatabaseRef.child(mCurrentUser.getUid()).removeValue();
                        mStorage.getReference().child("users/" + mCurrentUser.getUid() + "/" + AVATAR_FILE_NAME).delete();
                        mStore.collection("users").document(mCurrentUser.getUid()).delete();
                        mCurrentUser.delete();

                        //Logout
                        Log.d("TAG_LOG", getResources().getString(R.string.Delete_Success));
                        Toast.makeText(AccountActivity.this, R.string.Delete_Success, Toast.LENGTH_SHORT).show();

                        Logout();
                    }
                });

                //Abort Selection
                deleteAccountDialog.setNegativeButton(R.string.Cancel, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        Log.w(TAG_LOG, "Click Annulla");
                        Toast.makeText(AccountActivity.this, R.string.Toast_Abort, Toast.LENGTH_SHORT).show();
                    }
                });
                deleteAccountDialog.create().show();

            }
        });

        //Back Arrow Action on Click
        mBackArrow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        //Logout Btn Action on Click
        mLogoutBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Logout();
            }
        });

        //Password Edit Action on Click
        mPasswordEdit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                //Inflate Layout for personalizated Layout of AlertDialog
                View view = getLayoutInflater().inflate(R.layout.alert_dialog_layout, null);

                //Alert Dialog Build
                final AlertDialog.Builder passwordResetDialog = new AlertDialog.Builder(v.getContext());

                //Personalizated View Elements Assignment
                final EditText oldPassword = view.findViewById(R.id.OldPasswordEdit);
                final EditText newPassword = view.findViewById(R.id.NewPasswordEdit);
                passwordResetDialog.setView(view).setCancelable(false);

                //Confirmation Selection
                passwordResetDialog.setPositiveButton(R.string.Confirmation, new DialogInterface.OnClickListener() {

                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        String OldPasswordText = oldPassword.getText().toString();
                        String NewPasswordText = newPassword.getText().toString();
                        if (!OldPasswordText.isEmpty() && !NewPasswordText.isEmpty()) {
                            if (NewPasswordText.length() >= PASSWORD_LENGTH) {

                                if (!OldPasswordText.equals(NewPasswordText)) {
                                    //Update Password
                                    updatePassword(mCurrentUser.getEmail(), OldPasswordText, NewPasswordText);
                                } else {
                                    //oldPassword equals to new Password
                                    Toast.makeText(AccountActivity.this, getResources().getString(R.string.Update_Password_Fail) + "\n" + getResources().getString(R.string.New_Equals_Old_Password_Error), Toast.LENGTH_SHORT).show();
                                }

                            } else {
                                //new Password Length is < 6
                                Toast.makeText(AccountActivity.this, getResources().getString(R.string.Update_Password_Fail) + "\n" + getResources().getString(R.string.Password_Length), Toast.LENGTH_SHORT).show();
                            }
                        } else {
                            //oldPassword or/and newPassword isEmpty
                            Toast.makeText(AccountActivity.this, getResources().getString(R.string.Update_Password_Fail) + "\n" + getResources().getString(R.string.Both_Password_required_Error), Toast.LENGTH_SHORT).show();


                        }
                    }
                });

                //Abort Selection
                passwordResetDialog.setNegativeButton(R.string.Cancel, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        Toast.makeText(AccountActivity.this, R.string.Toast_Abort, Toast.LENGTH_SHORT).show();
                    }
                });

                //AlertDialog Show
                passwordResetDialog.create().show();
            }
        });

        //Email Edit Action on Click
        mEmailEdit.setOnClickListener(new View.OnClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.O)
            @Override
            public void onClick(View v) {

                //EditText settings build
                final EditText EmailEdit = new EditText(v.getContext());
                //Define new Typeface for set arial font
                Typeface tf = getResources().getFont(R.font.arial);
                EmailEdit.setTypeface(tf);
                EmailEdit.setInputType(InputType.TYPE_TEXT_VARIATION_EMAIL_ADDRESS);

                //AlertDialog Build
                final AlertDialog.Builder emailEditDialog = new AlertDialog.Builder(v.getContext());
                emailEditDialog.setTitle(R.string.Update_Email_Text);
                emailEditDialog.setMessage(R.string.Reset_Insert_Email);
                emailEditDialog.setView(EmailEdit);

                //Confirmation Selection
                emailEditDialog.setPositiveButton(R.string.Confirmation, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                        final String newEmail = EmailEdit.getText().toString();


                        if (!newEmail.equals(mCurrentUser.getEmail()) && !newEmail.isEmpty()) {
                            mCurrentUser.updateEmail(newEmail).addOnSuccessListener(new OnSuccessListener<Void>() {
                                @Override
                                public void onSuccess(Void aVoid) {
                                    //Update User Info and send Verification Email
                                    setUserInfo();
                                    mCurrentUser.sendEmailVerification();

                                    //Update Username on Firebase Realtime Database
                                    DatabaseReference DatabaseProfileRef= mDatabaseRef.child(mCurrentUser.getUid()).child("Profile");
                                    Map<String, Object> usernameUpdate = new HashMap<>();
                                    usernameUpdate.put("Email", newEmail);
                                    DatabaseProfileRef.updateChildren(usernameUpdate);


                                    Toast.makeText(AccountActivity.this, R.string.Update_Email_Success, Toast.LENGTH_SHORT).show();

                                }
                            }).addOnFailureListener(new OnFailureListener() {
                                @Override
                                public void onFailure(@NonNull Exception e) {
                                    Toast.makeText(AccountActivity.this, getResources().getString(R.string.Update_Email_Fail) + "\n" + e.getMessage(), Toast.LENGTH_SHORT).show();
                                }
                            });
                        } else {
                            if (newEmail.isEmpty()) {
                                //Email is Empty
                                Toast.makeText(AccountActivity.this, getResources().getString(R.string.Update_Email_Fail) + "\n" + getResources().getString(R.string.Email_Required), Toast.LENGTH_SHORT).show();

                            }
                            //The newEmail is equals to oldEmail
                            Toast.makeText(AccountActivity.this, getResources().getString(R.string.Update_Email_Fail) + "\n" + getResources().getString(R.string.New_Equals_Old_Email_Error), Toast.LENGTH_SHORT).show();

                        }
                    }
                });

                //Abort Selection
                emailEditDialog.setNegativeButton(R.string.Cancel, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        Toast.makeText(AccountActivity.this, R.string.Toast_Abort, Toast.LENGTH_SHORT).show();
                    }
                });

                //AlertDialog Show
                emailEditDialog.create().show();

            }
        });

        //Username Edit Action on Click
        mUsernameEdit.setOnClickListener(new View.OnClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.O)
            @Override
            public void onClick(View v) {

                //Edit Text Setting Build
                final EditText UsernameEdit = new EditText(v.getContext());
                Typeface tf = getResources().getFont(R.font.arial);
                UsernameEdit.setTypeface(tf);

                //AlertDialog Build
                final AlertDialog.Builder UsernameEditDialog = new AlertDialog.Builder(v.getContext());
                UsernameEditDialog.setTitle(R.string.Update_Username_Text);
                UsernameEditDialog.setMessage(R.string.Update_Insert_Username);
                UsernameEditDialog.setView(UsernameEdit);

                //Confirmation Selection
                UsernameEditDialog.setPositiveButton(R.string.Confirmation, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        final String newUsername = UsernameEdit.getText().toString();
                        if (!newUsername.isEmpty()) {
                            if (!newUsername.equals(mCurrentUser.getDisplayName())) {
                                //Update Username
                                UserProfileChangeRequest profileUpdates = new UserProfileChangeRequest.Builder().setDisplayName(newUsername).build();
                                mAuth.getCurrentUser().updateProfile(profileUpdates).addOnCompleteListener(new OnCompleteListener<Void>() {
                                            @Override
                                            public void onComplete(@NonNull Task<Void> task) {
                                                if (task.isSuccessful()) {
                                                    //Update User Info
                                                    setUserInfo();

                                                    //Update Username on Firebase Realtime Database
                                                    DatabaseReference DatabaseProfileRef = mDatabaseRef.child(mCurrentUser.getUid()).child("Profile");
                                                    Map<String, Object> usernameUpdate = new HashMap<>();
                                                    usernameUpdate.put("Username", newUsername);
                                                    DatabaseProfileRef.updateChildren(usernameUpdate);

                                                    Log.d(TAG_LOG, getResources().getString(R.string.Update_Username_Success));
                                                    Toast.makeText(AccountActivity.this, R.string.Update_Username_Success, Toast.LENGTH_SHORT).show();

                                                }
                                            }
                                        });
                            } else {
                                //oldUsername equals to newUsername
                                Toast.makeText(AccountActivity.this, getResources().getString(R.string.Update_Username_Fail) + "\n" + getResources().getString(R.string.New_Equals_Old_Username_Error), Toast.LENGTH_SHORT).show();

                            }
                        } else {
                            //Username is Empty
                            UsernameEdit.setError(getResources().getString(R.string.Username_Required));
                            Toast.makeText(AccountActivity.this, getResources().getString(R.string.Update_Username_Fail) + "\n" + getResources().getString(R.string.Username_Required), Toast.LENGTH_SHORT).show();

                        }
                    }
                });

                //Abort Selection
                UsernameEditDialog.setNegativeButton(R.string.Cancel, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        Toast.makeText(AccountActivity.this, R.string.Toast_Abort, Toast.LENGTH_SHORT).show();
                    }
                });

                //AlertDialog Show
                UsernameEditDialog.create().show();

            }
        });


        //Avatar Edit Action on Click
        mAvatarEdit.setOnClickListener(new View.OnClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.O)
            @Override
            public void onClick(View v) {
                //Define Dialog Items List
                final CharSequence[] items = {getResources().getString(R.string.Camera_Name), getResources().getString(R.string.Gallery_Name), getResources().getString(R.string.Preset_Avatar_Name), getResources().getString(R.string.Cancel)};

                //Title Setting Build
                TextView title = new TextView(getApplicationContext());
                title.setTypeface(getResources().getFont(R.font.fortnite));
                title.setText(R.string.Update_Profile_Image_Text);
                title.setPadding(30, 30, 0, 10);
                title.setTextSize(25);

                //AlertDialog Build
                final AlertDialog.Builder editImageDialog = new AlertDialog.Builder(v.getContext(), R.style.Arial_font);
                editImageDialog.setCustomTitle(title);

                //Dialo Item Select
                editImageDialog.setItems(items, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int i) {
                        //Camera Selection
                        if (items[i].equals(getResources().getString(R.string.Camera_Name))) {

                            //Check Permission
                            if (ContextCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.CAMERA) == PackageManager.PERMISSION_GRANTED) {

                                //Start implicit Intent for Camera
                                Intent camera = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                                if(camera.resolveActivity(getPackageManager())!= null) {
                                    startActivityForResult(camera, CAMERA_REQUEST_CODE);
                                }
                                else{
                                    //There Are not Application for this Request
                                    Toast.makeText(getApplicationContext(),getResources().getString(R.string.ActionApplicationNotFounded),Toast.LENGTH_SHORT).show();
                                }
                            } else {
                                //Require Camera Permission
                                requestPermission(Manifest.permission.CAMERA);
                            }
                        }

                        //Gallery Select
                        else if (items[i].equals(getResources().getString(R.string.Gallery_Name))) {

                            //Check Permission
                            if (ContextCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED) {

                                //Start implicit Intent for Read Picture time files from Storage
                                Intent openGalleryIntent = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI).setType(IMAGE_MIME_TYPE);
                                if(openGalleryIntent.resolveActivity(getPackageManager())!=null) {
                                    startActivityForResult(Intent.createChooser(openGalleryIntent, getResources().getString(R.string.Select_file)), GALLERY_PERMISSION_REQUEST_CODE);
                                }
                                else{
                                    //There Are not Application for this Request
                                    Toast.makeText(getApplicationContext(),getResources().getString(R.string.ActionApplicationNotFounded),Toast.LENGTH_SHORT).show();
                                }
                            } else {
                                //Require Read Storage Permission
                                requestPermission(Manifest.permission.READ_EXTERNAL_STORAGE);
                            }
                        }
                        //Default Avatar Select
                        else if (items[i].equals(getResources().getString(R.string.Preset_Avatar_Name))) {
                            //Set Application Default Avatar
                            BitmapDrawable bitmapDrawable = (BitmapDrawable) mStandardAvatar;
                            mProfilePic.setImageBitmap(bitmapDrawable.getBitmap());
                            Log.d(TAG_LOG, getResources().getString(R.string.Preset_Avatar_Name));

                            //Delete Personalized Avatar from Storage
                            mStorage.getReference().child("users/" + mAuth.getCurrentUser().getUid() + "/" + AVATAR_FILE_NAME).delete();

                        }

                        //Abort Select
                        else if (items[i].equals(getResources().getString(R.string.Cancel))) {
                            dialog.dismiss();
                        }
                    }
                });

                //AlertDialog Show
                editImageDialog.create().show();
            }
        });
    }

    //Logout Function --> disconnect from Firebase Account and go to Login
    private void Logout() {
        mAuth.signOut();
        goToLogin();
        finish();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @androidx.annotation.Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        //the Intent it went well
        if (resultCode == Activity.RESULT_OK) {

            //Return Gallery Request Code
            if (requestCode == GALLERY_PERMISSION_REQUEST_CODE) {
                Uri imgUri = data.getData();
                try {
                    //Set Avatar Image with Gallery selected Picture
                    Bitmap galleryImg = MediaStore.Images.Media.getBitmap(getContentResolver(), imgUri);
                    mProfilePic.setImageBitmap(galleryImg);

                    //Upload Gallery selected Picture on Firebase Storage
                    uploadSaveImage(galleryImg);

                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            //Return Camera Request Code
            if (requestCode == CAMERA_REQUEST_CODE) {
                //get Image from bundle and set Avatar with image
                Bundle extras = data.getExtras();
                Bitmap bitmapImage = (Bitmap) extras.get("data");
                mProfilePic.setImageBitmap(bitmapImage);

                //Upload Camera Picture on Firebase Storage
                uploadSaveImage(bitmapImage);
            }
        }
    }

    //method for uploading the avatar to the firebase storage
    private void uploadSaveImage(Bitmap bitmapImage) {
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();

        //Storage Path
        final StorageReference storageRef = mStorage.getReference().child("users/" + mAuth.getCurrentUser().getUid() + "/" + AVATAR_FILE_NAME);

        //Image Compression on Byte
        bitmapImage.compress(Bitmap.CompressFormat.JPEG, 100, bytes);
        final byte[] image = bytes.toByteArray();

        //Upload image Bytes sequence on Firebase Storage
        UploadTask uploadTask = storageRef.putBytes(image);
        uploadTask.addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception exception) {
                Log.d(TAG_LOG, getResources().getString(R.string.Upload_Avatar_Fail));
            }
        }).addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
            @Override
            public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                Log.d(TAG_LOG, getResources().getString(R.string.Upload_Avatar_Success));
            }
        });
    }


    @Override
    public void onBackPressed() {
        finish();
    }

    //Function that implements the data loading from the Database to the activity (Username, Email and Avatar)
    protected void setUserInfo() {
        DocumentReference documentReference = mStore.collection("users").document(mCurrentUser.getUid());
        documentReference.addSnapshotListener(this, new EventListener<DocumentSnapshot>() {
            @Override
            public void onEvent(@Nullable DocumentSnapshot documentSnapshot, @Nullable FirebaseFirestoreException e) {
                if (FirebaseAuth.getInstance().getCurrentUser() != null) {
                    mUsername.setText(mCurrentUser.getDisplayName());
                    mEmail.setText(mCurrentUser.getEmail());
                }
            }
        });

        setVerifyEmailStatus();
        setUserAvatar();

    }

    //Verify Verification Status of Account
    private void setVerifyEmailStatus() {
        if (mAuth.getCurrentUser().isEmailVerified()) {
            TextView VerifyText = findViewById(R.id.VerifyText);
            VerifyText.setText(R.string.Account_Verified);
            VerifyText.setTextColor(getResources().getColor(R.color.green_verify));
        }
    }

    //Set User Avatar by Firebase Storage if this is Available
    protected void setUserAvatar() {
        StorageReference profileRef = mStorage.getReference().child("users/" + mAuth.getCurrentUser().getUid() + "/" + AVATAR_FILE_NAME);
        profileRef.getDownloadUrl().addOnSuccessListener(new OnSuccessListener<Uri>() {
            @Override
            //Avatar Founded
            public void onSuccess(Uri uri) {
                Picasso.get().load(uri).into(mProfilePic);
            }
        }).addOnFailureListener(new OnFailureListener() {
            @Override
            //Avatar not Founded
            public void onFailure(@NonNull Exception e) {
                Log.d(TAG_LOG, "There arent Personalized Avatar");
            }
        });
    }

    //Method that allows you to go to loginActivity and close active activities
    public void goToLogin() {
        Intent intent = new Intent(this, LoginActivity.class);
        //Standard Flags for Clear Stack of Activity
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(intent);
        finish();
    }

    //Method to Update password
    private void updatePassword(String email, String oldPwd, final String newPwd) {
        //Create credential with email and oldPassword for reauthenticate account
        AuthCredential credential = EmailAuthProvider.getCredential(email, oldPwd);
        mCurrentUser.reauthenticate(credential).addOnCompleteListener(new OnCompleteListener<Void>() {
            @Override
            public void onComplete(@NonNull Task<Void> task) {
                if (task.isSuccessful()) {
                    //Update Account Password
                    mCurrentUser.updatePassword(newPwd).addOnCompleteListener(new OnCompleteListener<Void>() {
                        @Override
                        public void onComplete(@NonNull Task<Void> task) {
                            Toast.makeText(AccountActivity.this, R.string.Update_Password_Success, Toast.LENGTH_SHORT).show();
                        }
                    });
                }
            }
        }).addOnFailureListener(new OnFailureListener() {
            //On failed Task
            @Override
            public void onFailure(@NonNull Exception e) {
                Toast.makeText(AccountActivity.this, R.string.Old_Password_Error, Toast.LENGTH_SHORT).show();

            }
        });
    }

    //Method to request Permission
    private void requestPermission(final String Permission) {
        if (ActivityCompat.shouldShowRequestPermissionRationale(this, Permission)) {
            int requestCode = getRequestCode(Permission);
            ActivityCompat.requestPermissions(AccountActivity.this, new String[]{Permission}, requestCode);
        } else {
            ActivityCompat.requestPermissions(this, new String[]{Permission}, getRequestCode(Permission));
        }
    }

    //Method that return Permission Request Code
    private int getRequestCode(String Permission) {
        switch (Permission) {
            case Manifest.permission.CAMERA:
                return CAMERA_REQUEST_CODE;
            default:
                return READ_PERMISSION_REQUEST_CODE;
        }
    }

    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        //Camera Request Code
        if (requestCode == CAMERA_REQUEST_CODE) {

            //If Permission are garanted start intent
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                Toast.makeText(this, getResources().getString(R.string.Permission_granted), Toast.LENGTH_SHORT).show();

                //Start Camera Intent
                Intent camera = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                if(camera.resolveActivity(getPackageManager())!= null) {
                    startActivityForResult(camera, CAMERA_REQUEST_CODE);
                }
                else{
                    //There Are not Application for this Request
                    Toast.makeText(getApplicationContext(),getResources().getString(R.string.ActionApplicationNotFounded),Toast.LENGTH_SHORT).show();
                }
            } else {
                Toast.makeText(this, getResources().getString(R.string.Permission_denied), Toast.LENGTH_SHORT).show();
            }
        }

        //Gallery Request Code
        if (requestCode == READ_PERMISSION_REQUEST_CODE) {

            //If Permission are garanted start intent
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                Toast.makeText(this, getResources().getString(R.string.Permission_granted), Toast.LENGTH_SHORT).show();

                //Start Gallery Intent
                Intent openGalleryIntent = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI).setType(IMAGE_MIME_TYPE);
                if(openGalleryIntent.resolveActivity(getPackageManager())!= null) {
                    startActivityForResult(Intent.createChooser(openGalleryIntent, getResources().getString(R.string.Select_file)), GALLERY_PERMISSION_REQUEST_CODE);
                }
                else{
                    //There Are not Application for this Request
                    Toast.makeText(getApplicationContext(),getResources().getString(R.string.ActionApplicationNotFounded),Toast.LENGTH_SHORT).show();
                }
            } else {
                Toast.makeText(this, getResources().getString(R.string.Permission_denied), Toast.LENGTH_SHORT).show();
            }
        }
    }
}