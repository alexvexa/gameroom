package com.e.gameroom;

import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.e.gameroom.database.DatabaseHelper;
import com.e.gameroom.games.breakout.BreakoutActivity;
import com.e.gameroom.games.catchtheball.CatchTheBallActivity;
import com.e.gameroom.games.snake.SnakeActivity;
import com.e.gameroom.games.spaceinv.SpaceInvadersActivity;
import com.google.android.material.navigation.NavigationView;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.viewpager.widget.ViewPager;


public class HomeActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener {
    private static final String TAG_LOG = HomeActivity.class.getName();
    private static final String TEXT_MIME_TYPE ="text/plain";

    //Firebase Connect
    private FirebaseAuth mAuth;
    private FirebaseUser mCurrentUser;
    private FirebaseDatabase mFirebaseDb;
    private DatabaseReference mDatabaseRef;

    //Database Helper
    private DatabaseHelper mDatabaseHelper;

    //Interface Elements Reference (Layout)
    private Menu mMenu;
    private GameAdapter mGameAdapter;
    private ImageView mLeftArrow, mRightArrow, mMenuBtn;
    private ViewPager mViewPager;
    private MenuItem mAccountMenuItem, mLoginMenuItem;
    private List<Games> mGameList;
    private NavigationView mNavigationView;
    private DrawerLayout mDrawerLayout;
    private Button mPlayBtn;
    private TextView mGameTitle, mPersonalScore, mGameHighScore;

    private Long mBestScore, mGameBestScore;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);

        //Enabling Firebase Connection
        mAuth = FirebaseAuth.getInstance();
        mCurrentUser = mAuth.getCurrentUser();

        //Invalidating Menu Options for Guest
        invalidateOptionsMenu();

        //Elements assignment
        mDatabaseHelper = new DatabaseHelper(getApplicationContext());
        mLeftArrow = findViewById(R.id.LeftArrow);
        mRightArrow = findViewById(R.id.RightArrow);
        mNavigationView = findViewById(R.id.NavigationView);
        mMenuBtn = findViewById(R.id.imageMenu);
        mViewPager = findViewById(R.id.ViewPager);
        mNavigationView.setNavigationItemSelectedListener(this);
        mMenu = mNavigationView.getMenu();
        mPlayBtn = findViewById(R.id.Play);
        mGameTitle = findViewById(R.id.GameName);
        mDrawerLayout = findViewById(R.id.drawer_layout);
        mPersonalScore = findViewById(R.id.personal_scores);
        mGameHighScore = findViewById(R.id.bestScore);
        mNavigationView.setItemIconTintList(null);
        mNavigationView.setItemIconPadding(20);

        //Hiding menu from State
        if (mCurrentUser == null) {
            mAccountMenuItem = mMenu.findItem(R.id.Account).setVisible(false);
        } else {
            mLoginMenuItem = mMenu.findItem(R.id.Login).setVisible(false);

        }

        //MenuBtn Action on Click
        mMenuBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mDrawerLayout.isDrawerOpen(GravityCompat.START)) {
                    drawerClosing();

                } else {
                    drawerOpening();
                }
            }
        });

        //Games ArrayList population
        mGameList = new ArrayList<>();
        mGameList.add((new Games(R.drawable.catch_the_ball_icon, getResources().getString(R.string.catch_the_ball_Name))));
        mGameList.add((new Games(R.drawable.snake_icon, getResources().getString(R.string.Snake_Game_Name))));
        mGameList.add((new Games(R.drawable.breakout_icon, getResources().getString(R.string.Breakout_Game_Name))));
        mGameList.add((new Games(R.drawable.spaceinvaders_icon, getResources().getString(R.string.SpaceInvaders_Game_Name))));
        mGameAdapter = new GameAdapter(mGameList, getApplicationContext());

        mViewPager.setAdapter(mGameAdapter);

        //Set Games Info
        setGameTitle();
        setPersonalBestScore(mGameTitle.getText().toString());
        setGameHighScore(mGameTitle.getText().toString());

        mViewPager.setOnScrollChangeListener(new View.OnScrollChangeListener() {
            @Override
            public void onScrollChange(View v, int scrollX, int scrollY, int oldScrollX, int oldScrollY) {
                setGameTitle();
                setPersonalBestScore(mGameTitle.getText().toString());
                mGameBestScore = null;
                setGameHighScore(mGameTitle.getText().toString());

                if (mViewPager.getCurrentItem() == 0) {
                    mLeftArrow.setVisibility(View.INVISIBLE);

                } else {
                    mLeftArrow.setVisibility(View.VISIBLE);
                }

                if (mViewPager.getCurrentItem() == mGameList.size() - 1) {
                    mRightArrow.setVisibility(View.INVISIBLE);
                } else {
                    mRightArrow.setVisibility(View.VISIBLE);
                }
            }
        });

        //Play Btn Action on Click
        mPlayBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int currentItem = mViewPager.getCurrentItem();
                GameAdapter adapterGame = (GameAdapter) mViewPager.getAdapter();
                String actualGame = adapterGame.getPositionTitle(currentItem);


                if (actualGame.equals(getResources().getString(R.string.catch_the_ball_Name))) {
                    Intent gameStart = new Intent(getApplicationContext(), CatchTheBallActivity.class);
                    startActivity(gameStart);
                }


                if (actualGame.equals(getResources().getString(R.string.Snake_Game_Name))) {
                    Intent gameStart = new Intent(getApplicationContext(), SnakeActivity.class);
                    startActivity(gameStart);
                }

                if (actualGame.equals(getResources().getString(R.string.SpaceInvaders_Game_Name))) {
                    Intent gameStart = new Intent(getApplicationContext(), SpaceInvadersActivity.class);
                    startActivity(gameStart);
                }

                if (actualGame.equals(getResources().getString(R.string.Breakout_Game_Name))) {
                    Intent gameStart = new Intent(getApplicationContext(), BreakoutActivity.class);
                    startActivity(gameStart);
                }
            }
        });

        mLeftArrow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mViewPager.arrowScroll(View.FOCUS_LEFT);
            }
        });

        mRightArrow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mViewPager.arrowScroll(View.FOCUS_RIGHT);
            }
        });
    }


    //Menu Item Selector
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        int id = item.getItemId();
        Intent activity;
        switch (id) {
            case R.id.Account:
                activity = new Intent(HomeActivity.this, AccountActivity.class);
                startActivity(activity);
                drawerClosing();
                break;


            case R.id.Scores:
                activity = new Intent(HomeActivity.this, ScoresActivity.class);
                startActivity(activity);
                drawerClosing();
                break;

            case R.id.Login:
                activity = new Intent(HomeActivity.this, LoginActivity.class);
                startActivity(activity);
                drawerClosing();
                finish();
                break;

            case R.id.Share:
                activity = new Intent();
                activity.setAction(Intent.ACTION_SEND);
                activity.putExtra(Intent.EXTRA_TEXT, getResources().getString(R.string.ShareString));
                activity.setType(TEXT_MIME_TYPE);
                Intent shareIntent = Intent.createChooser(activity, null);
                if(activity.resolveActivity(getPackageManager())!= null) {
                    startActivity(shareIntent);
                }
                else{
                    Toast.makeText(getApplicationContext(),getResources().getString(R.string.ActionApplicationNotFounded),Toast.LENGTH_SHORT).show();

                }

        }
        return false;
    }

    @Override
    public void onBackPressed() {
        if (mDrawerLayout.isDrawerOpen(GravityCompat.START)) {
            drawerClosing();

        } else {
            super.onBackPressed();
        }
    }


    @Override
    protected void onRestart() {
        super.onRestart();
    }

    private void setGameTitle() {
        int CurrentItem = mViewPager.getCurrentItem();
        GameAdapter adapterGame = (GameAdapter) mViewPager.getAdapter();
        mGameTitle.setText(adapterGame.getPositionTitle(CurrentItem));
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        Log.d(TAG_LOG, "HOME-ACTIVITY: restore state");
        super.onRestoreInstanceState(savedInstanceState);
        mViewPager.setCurrentItem(savedInstanceState.getInt("Position"));
        setScrollView();
        setPersonalBestScore(mGameTitle.getText().toString());
        setGameHighScore(mGameTitle.getText().toString());

    }


    @Override
    protected void onSaveInstanceState(Bundle outState) {
        Log.d(TAG_LOG, "HOME-ACTIVITY: salvataggio dello stato");
        super.onSaveInstanceState(outState);
        outState.putInt("Position", mViewPager.getCurrentItem());
    }

    protected void setScrollView() {
        setGameTitle();

        if (mViewPager.getCurrentItem() == 0) {
            mLeftArrow.setVisibility(View.INVISIBLE);

        } else {
            mLeftArrow.setVisibility(View.VISIBLE);
        }

        if (mViewPager.getCurrentItem() == mGameList.size() - 1) {
            mRightArrow.setVisibility(View.INVISIBLE);
        } else {
            mRightArrow.setVisibility(View.VISIBLE);
        }
    }


    private void drawerClosing() {
        mDrawerLayout.closeDrawer(GravityCompat.START);
        mMenuBtn.setImageDrawable(getResources().getDrawable(R.drawable.menu_yellow, null));
    }

    private void drawerOpening() {
        mDrawerLayout.openDrawer(GravityCompat.START);
        mMenuBtn.setImageDrawable(getResources().getDrawable(R.drawable.ic_close_black_24dp, null));

    }

    //Setting personalBestScore from Firebase OR SQLite Database
    public void setPersonalBestScore(final String game) {
        mAuth = FirebaseAuth.getInstance();
        mCurrentUser = mAuth.getCurrentUser();

        //Searching on Firebase
        if (mCurrentUser != null) {
            mFirebaseDb = FirebaseDatabase.getInstance();
            mDatabaseRef = mFirebaseDb.getReference().child("users/");
            mDatabaseRef.addValueEventListener(new ValueEventListener() {
                @Override
                public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                    mBestScore = dataSnapshot.child(mCurrentUser.getUid()).child("scores").child(game).getValue(Long.class);
                    if (mBestScore != null) {
                        mPersonalScore.setText(getResources().getString(R.string.personalBestScore) + " " + mBestScore.toString());
                    } else {
                        mPersonalScore.setText("");
                    }
                }

                @Override
                public void onCancelled(@NonNull DatabaseError databaseError) {

                }
            });

            //Searching on SQLite database
        } else {
            boolean founded = false;
            Cursor res = mDatabaseHelper.getAllData();
            while (res.moveToNext()) {
                if (res.getString(2).equals(game) && !founded) {
                    founded = true;
                    mPersonalScore.setText(getResources().getString(R.string.personalBestScore) + " " + res.getString(3));
                    Log.d("LOCAL SCORE", res.getString(3));
                }
            }
            if(!founded){
                mPersonalScore.setText(getResources().getString(R.string.noScore));
            }
        }
    }


    //Setting HighScore from Firebase
    public void setGameHighScore(final String Game) {
        mGameBestScore = null;
        mAuth = FirebaseAuth.getInstance();
        mCurrentUser = mAuth.getCurrentUser();
        if (mCurrentUser != null) {
            mFirebaseDb = FirebaseDatabase.getInstance();
            mDatabaseRef = mFirebaseDb.getReference().child("users/");
            mDatabaseRef.addValueEventListener(new ValueEventListener() {
                @Override
                public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                    for (DataSnapshot userData : dataSnapshot.getChildren()) {
                        Map<String, Object> userProfile = (Map<String, Object>) userData.getValue();
                        Map<String, Object> userScores = (Map<String, Object>) userProfile.get("scores");
                        if (userScores != null) {
                            Long gameScore = (Long) userScores.get(Game);
                            if (gameScore != null) {

                                if (mGameBestScore == null) {
                                    mGameBestScore = gameScore;

                                } else {

                                    if (gameScore > mGameBestScore) {
                                        mGameBestScore = gameScore;
                                    }
                                }
                                mGameHighScore.setText(getResources().getString(R.string.bestScore) + " " + mGameBestScore);
                                Log.d("Best Score", mGameBestScore.toString());
                            }
                        }
                        if (mGameBestScore == null) {
                            mGameHighScore.setText(getResources().getString(R.string.noScore));

                        }
                    }
                }

                @Override
                public void onCancelled(@NonNull DatabaseError databaseError) {

                }
            });

        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        setPersonalBestScore(mGameTitle.getText().toString());
    }
}
