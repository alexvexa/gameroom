package com.e.gameroom;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.GenericTypeIndicator;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Map;
import java.util.TreeMap;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.ListFragment;

public class ScoresFragment extends ListFragment {

    public static String TAG_LOG = ScoresFragment.class.getName();

    private ListView mListView;
    private View mScoreView;
    private String mGameName;

    //ArrayList where the data read from the database will be stored
    private ArrayList<String> mUsernames;
    private ArrayList<Long> mScores;
    private TreeMap<Long, String> mOrderedScores;

    //Defines the structure of the single Item in the list (Single Score)
    ScoreArrayAdapter scoreArrayAdapter;

    //Firebase Connect
    private FirebaseDatabase mDatabase = FirebaseDatabase.getInstance();
    private DatabaseReference mRefDatabase;

    public ScoresFragment() {
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mListView = getListView();
    }

    @Override
    public void onStart() {
        super.onStart();

        //Save the fragment status upon configuration change
        setRetainInstance(true);

        //Array Initialization
        mUsernames = new ArrayList<>();
        mScores = new ArrayList<>();
        mOrderedScores = new TreeMap(Collections.<Integer>reverseOrder());

        //set Database Root
        mRefDatabase = mDatabase.getReference("users");

        final GenericTypeIndicator<Map<String, Object>> scoreType = new GenericTypeIndicator<Map<String, Object>>() {
            @Override
            public int hashCode() {
                return super.hashCode();
            }
        };

        mRefDatabase.addChildEventListener(new ChildEventListener() {
            @Override
            public void onChildAdded(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {

                if (dataSnapshot.exists()) {
                    Map<String, Object> usersId = dataSnapshot.getValue(scoreType);
                    Map<String, Object> profileInfo = (Map<String, Object>) usersId.get("Profile");

                    Map<String, Object> userScores = (Map<String, Object>) usersId.get("scores");
                    if (userScores != null) {

                        //Allowing to view the scores of all single-user games
                        Log.d(TAG_LOG, "Username: " + profileInfo.get("Username") + " UsersScores" + userScores.toString());

                        //For each user score, save username and score info
                        for (int i = 0; i < userScores.size(); i++) {
                            Long score = (Long) userScores.get(mGameName);

                            if (score != null) {
                                String username = profileInfo.get("Username").toString();
                                mUsernames.add(username);
                                mScores.add(score);
                                mOrderedScores.put(score, username);
                            }
                        }
                    }
                }
                //OrderedScore contains all scores in the association Score=Username es. 1000 = User
                Log.d(TAG_LOG, "OrderedScores : " + mOrderedScores.toString());
                mUsernames.clear();
                mScores.clear();

                //Scores and Username Arrays sorted in descending order
                mScores = new ArrayList<>(mOrderedScores.keySet());
                mUsernames = new ArrayList<>(mOrderedScores.values());

                //Log to see Scores and Usernames List ordered (Desc) for Score
                Log.d(TAG_LOG, "Scores: " + mScores.toString());
                Log.d(TAG_LOG, "Usernames: " + mUsernames.toString());

                scoreArrayAdapter = new ScoreArrayAdapter(getActivity(), R.layout.item_score, mUsernames, mScores);
                scoreArrayAdapter.notifyDataSetChanged();
                mListView.setAdapter(scoreArrayAdapter);

            }

            @Override
            public void onChildChanged(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {

            }

            @Override
            public void onChildRemoved(@NonNull DataSnapshot dataSnapshot) {

            }

            @Override
            public void onChildMoved(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {

            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        mScoreView = inflater.inflate(R.layout.scorelist_layout, container, false);
        return mScoreView;
    }


    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
    }

    //Method that instantiates the list of Scores
    public static ScoresFragment newInstance(String Game_Name) {
        Bundle args = new Bundle();
        ScoresFragment fragment = new ScoresFragment();
        fragment.mGameName = Game_Name;
        fragment.setArguments(args);
        return fragment;
    }

}
