package com.e.gameroom;

import android.content.Context;
import android.content.Intent;
import android.graphics.Paint;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.textfield.TextInputLayout;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.UserProfileChangeRequest;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.firestore.FirebaseFirestore;

import java.util.HashMap;
import java.util.Map;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

public class RegisterActivity extends AppCompatActivity {
    private static final String TAG_LOG = RegisterActivity.class.getName();
    private static final int PASSWORD_LENGTH = 6;
    private static final String EMAIL_AT= "@";

    //Firebase Connect
    private FirebaseAuth mAuth;
    private FirebaseDatabase mDatabase;
    private FirebaseUser mCurrentUser;

    //Interface Elements
    private EditText mUsername, mEmail;
    private EditText mPassword;
    private Button mRegisterBtn;
    private TextView mLoginLink;
    private ProgressBar mProgressBar;
    private TextInputLayout mUsernameLayout, mEmailLayout, mPasswordLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);

        //Elements assignment
        mUsername = findViewById(R.id.username_register);
        mEmail = findViewById(R.id.email_register);
        mPassword = findViewById(R.id.password_register);
        mRegisterBtn = findViewById(R.id.RegisterButton);
        mLoginLink = findViewById(R.id.goToLogin);
        mProgressBar = findViewById(R.id.progressBarRegister);
        mUsernameLayout = findViewById(R.id.UsernameLayout);
        mEmailLayout = findViewById(R.id.EmailLayout);
        mPasswordLayout = findViewById(R.id.PasswordLayout);

        //Firebase initialization
        mAuth = FirebaseAuth.getInstance();
        mDatabase = FirebaseDatabase.getInstance();

        //TextView Underline
        mLoginLink.setPaintFlags(Paint.UNDERLINE_TEXT_FLAG);

        //Registrer Btn Action on Click
        mRegisterBtn.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                //Clean layouts Error
                mUsernameLayout.setError(null);
                mEmailLayout.setError(null);
                mPasswordLayout.setError(null);

                final String email = mEmail.getText().toString().trim();
                String password = mPassword.getText().toString().trim();
                final String username = mUsername.getText().toString().trim();

                //Email and Password check
                if (TextUtils.isEmpty(username)) {
                    mUsernameLayout.setError(getResources().getString(R.string.Username_Required));
                    return;
                }
                if (TextUtils.isEmpty(email)) {
                    mEmailLayout.setError(getResources().getString(R.string.Email_Required));
                    return;
                } else {
                    if (!email.contains(EMAIL_AT)) {
                        mEmailLayout.setError(getResources().getString(R.string.Wrong_Format));
                        return;
                    }
                }
                if (TextUtils.isEmpty(password)) {
                    mPasswordLayout.setError(getResources().getString(R.string.Password_Required));
                    return;
                }

                if (password.length() < PASSWORD_LENGTH) {
                    mPasswordLayout.setError(getResources().getString(R.string.Password_Length));
                    return;
                }

                //Keyboard input closing
                InputMethodManager inputManager = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
                inputManager.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(),InputMethodManager.HIDE_NOT_ALWAYS);

                //ProgressBar set Visibility = VISIBLE
                mProgressBar.setVisibility(View.VISIBLE);

                if (!(mPassword.getText().toString().isEmpty()) && !(mEmail.getText().toString().isEmpty())) {

                    mAuth.createUserWithEmailAndPassword(email, password).addOnCompleteListener(new OnCompleteListener<AuthResult>() {
                        @Override
                        public void onComplete(@NonNull Task<AuthResult> task) {
                            if (task.isSuccessful()) {
                                mCurrentUser = mAuth.getCurrentUser();

                                //Set Account Display Name
                                UserProfileChangeRequest profileUpdates = new UserProfileChangeRequest.Builder()
                                        .setDisplayName(username)
                                        .build();
                                mCurrentUser.updateProfile(profileUpdates);

                                //Upload Profile Account Info on Firebase
                                Map<String, Object> profileData = new HashMap<>();
                                profileData.put("Username", username);
                                profileData.put("Email", email);
                                String path = "users/" + mCurrentUser.getUid() + "/Profile";
                                mDatabase.getReference(path).updateChildren(profileData);

                                mCurrentUser.sendEmailVerification().addOnCompleteListener(new OnCompleteListener<Void>() {
                                    @Override
                                    public void onComplete(@NonNull Task<Void> task) {
                                        if (task.isSuccessful()) {
                                            Log.d(TAG_LOG, "Verification Email sent");
                                            Toast.makeText(RegisterActivity.this, R.string.Toast_Account_Created, Toast.LENGTH_SHORT).show();
                                        }
                                    }
                                });

                                //Authentication Success --> Go to LoginActivity
                                Intent homeIntent = new Intent(getApplicationContext(), LoginActivity.class);
                                startActivity(homeIntent);

                                //ProgressBar set Visibility = INVISIBLE
                                mProgressBar.setVisibility(View.INVISIBLE);
                                finish();

                            } else {
                                Toast.makeText(RegisterActivity.this, "Error!" + task.getException().getMessage(), Toast.LENGTH_SHORT).show();

                                //ProgressBar set Visibility = INVISIBLE
                                mProgressBar.setVisibility(View.INVISIBLE);
                            }
                        }
                    });
                } else {
                    //ProgressBar set Visibility = INVISIBLE
                    mProgressBar.setVisibility(View.INVISIBLE);
                }
            }
        });

        //Login Link Action on Click
        mLoginLink.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }
}
