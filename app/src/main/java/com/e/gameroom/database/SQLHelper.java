package com.e.gameroom.database;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.e.gameroom.database.GameroomContract.*;
public class SQLHelper extends SQLiteOpenHelper {

    //Var Definitions (Only onCreate & onUpgrage)

    public static final String DATABASE_NAME = "GameRoomScore.db";

    public static final String CREATE_TABLE = "CREATE TABLE ";
    public static final String TEXT_TYPE = " TEXT ";
    public static final String COMMA = ",";
    public static final String START_BRACKET =" (";
    public static final String END_BRACKET =" )";
    public static final String PRIMARY_KEY = " PRIMARY KEY AUTOINCREMENT ";

    public static final String INTEGER_TYPE = " INTEGER ";
    public static final String TABLE_STRUCT =
            START_BRACKET+
            GameEntry.ID + INTEGER_TYPE +  PRIMARY_KEY + COMMA +
            GameEntry.USER + TEXT_TYPE + COMMA +
            GameEntry.GAME_NAME + TEXT_TYPE + COMMA +
            GameEntry.SCORE + INTEGER_TYPE +
            END_BRACKET;

    public SQLHelper(Context context) {
        super(context, DATABASE_NAME, null, 1);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(CREATE_TABLE + GameEntry.TABLE_NAME + TABLE_STRUCT);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS " + GameEntry.TABLE_NAME);
    }
}
