package com.e.gameroom.database;

import android.provider.BaseColumns;

public class GameroomContract {

    //Tables Model
    GameroomContract() {
    }

    //SQLite GameScores Table Structure
    public static final class GameEntry implements BaseColumns {
        public static final String TABLE_NAME = "Score_table";
        public static final String ID = "ID";
        public static final String USER = "USER";
        public static final String GAME_NAME = "NAME_GAME";
        public static final String SCORE = "SCORE";
    }
}