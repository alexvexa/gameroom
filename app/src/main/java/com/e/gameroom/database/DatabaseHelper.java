package com.e.gameroom.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.util.Log;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.GenericTypeIndicator;
import com.google.firebase.database.ValueEventListener;

import java.util.HashMap;
import java.util.Map;

import androidx.annotation.NonNull;

import com.e.gameroom.database.GameroomContract.*;



public class DatabaseHelper {

    //Firebase Connect + Query + Values Update

    FirebaseAuth mAuth;
    FirebaseUser currentUser;
    FirebaseDatabase FirebaseDb;
    DatabaseReference mDatabaseRef;
    SQLHelper SQLiteOpenHelper;

    public static final String SELECT_ALL = "SELECT * FROM ";
    public static final String ORDER_BY = " ORDER BY ";
    public static final String DESC = " DESC ";

    //DatabaseHelper Constructor
    public DatabaseHelper(Context context){
        SQLiteOpenHelper = new SQLHelper(context);
    }

    // Saving the new score on Firebase
    public void insertNewScore(String Game_Id, int score) {
        //Firebase instantiation
        mDatabaseRef = FirebaseDb.getReference();
        mAuth = FirebaseAuth.getInstance();
        currentUser = mAuth.getCurrentUser();

        //userId is the uid of current user
        // check no user logged or registered
        Map<String, Object> gameData = new HashMap<>();
        gameData.put(Game_Id, score);

        //Save score on path Children
        mDatabaseRef.child("users")
                .child(currentUser.getUid())
                .child("scores")
                .updateChildren(gameData);

        mDatabaseRef.keepSynced(true);
    }

    //Function that updates the scores on the database (Local or Firebase)
    public void updateScore(final String game_name, final int newscore, String Username) {
        FirebaseDb = FirebaseDatabase.getInstance();
        mAuth = FirebaseAuth.getInstance();
        currentUser = mAuth.getCurrentUser();

        //Data saving on Firebase
        if (currentUser != null) {

            //Reference on Database for read oldScore and Update with new Score
            final String path = "users/" + currentUser.getUid() + "/scores";
            mDatabaseRef = FirebaseDb.getReference(path);

            final GenericTypeIndicator<Map<String, Object>> scoreType = new GenericTypeIndicator<Map<String, Object>>() {
                @Override
                public int hashCode() {
                    return super.hashCode();
                }
            };

            mDatabaseRef.addValueEventListener(new ValueEventListener() {
                @Override
                public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                    if (dataSnapshot.exists()) {

                        //Get oldScore from Firebase Database
                        Map<String, Object> oldScoreMap = dataSnapshot.getValue(scoreType);
                        Long oldScore = (Long) oldScoreMap.get(game_name);
                        if (oldScore != null) {
                            if (oldScore < newscore) {
                                //Update oldScore
                                Log.d("TAG", "Old score: " + oldScore);
                                Log.d("TAG", "New score: " + newscore);
                                Map<String, Object> NewScoreMap = new HashMap<>();
                                NewScoreMap.put(game_name, newscore);
                                mDatabaseRef.updateChildren(NewScoreMap);
                            } else {
                                Log.d("TAG", "Il tuo High Score è più alto dello score ottenuto");
                            }
                        } else {
                            Log.d("TAG", "new Score Insert");
                            //If there aren't personal score for the game, insert new Score
                            insertNewScore(game_name, newscore);
                        }

                    } else {
                        //If there aren't personal score for all the games, insert new Score and create folder
                        Log.d("TAG", "new Score Insert");
                        insertNewScore(game_name, newscore);
                    }
                }

                @Override
                public void onCancelled(@NonNull DatabaseError databaseError) {

                }
            });
            mDatabaseRef.keepSynced(true);
        }

        //Not registered User --> Saving Scores on SQLite Database
        else {
            //Construct value to save on Database
            ContentValues contentValue = new ContentValues();
            contentValue.put(GameEntry.USER, Username);
            contentValue.put(GameEntry.GAME_NAME, game_name);
            contentValue.put(GameEntry.SCORE, newscore);

            //Insert on Database the value of Realized Score
            long result = SQLiteOpenHelper.getWritableDatabase().insert(GameEntry.TABLE_NAME, null, contentValue);

            if (result == -1) {
                Log.d("TAG_LOG", "Salvataggio score in locale fallito");
            } else {
                Log.d("TAG_LOG", "Salvataggio score in locale avvenuto con successo");
            }
        }
    }

    //method that performs a query that returns all scores of all games of the SQLite database
    public Cursor getAllData() {
        Cursor res = SQLiteOpenHelper.getReadableDatabase().rawQuery(SELECT_ALL + GameEntry.TABLE_NAME + ORDER_BY + GameEntry.SCORE + DESC, null);
        return res;
    }

}
