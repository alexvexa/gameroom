package com.e.gameroom.games.snake;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Point;
import android.graphics.RectF;
import android.media.MediaPlayer;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.Display;
import android.view.MotionEvent;
import android.view.SurfaceHolder;
import android.view.SurfaceView;

import com.e.gameroom.R;
import com.e.gameroom.games.GameScoreActivity;

import java.util.Random;

import androidx.annotation.RequiresApi;

public class SnakeActivity extends Activity {
    Canvas canvas;
    SnakeView snakeView;
    Bitmap headBitmap;
    Bitmap bodyBitmap;
    Bitmap tailBitmap;

    Bitmap appleBitmap;


    float startX;
    float startY;
    float endX;
    float endY;
    int directionOfTravel = 0;
    //0 = up, 1 = right, 2 = down, 3= left

    int screenWidth;
    int screenHeight;
    int topGap;
    int lateralGap;
    int bottomGap;
    //stats
    long lastFrameTime;
    int fps;
    int score;

    int[] snakeX;
    int[] snakeY;
    int snakeLength;
    int appleX;
    int appleY;

    int blockSize;
    int numBlocksWide;
    int numBlocksHigh;

    MediaPlayer music;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        music = MediaPlayer.create(this, R.raw.snake_song);
        music.setLooping(true);
        music.start();


        configureDisplay();
        snakeView = new SnakeView(this);
        setContentView(snakeView);
    }

    class SnakeView extends SurfaceView implements Runnable {
        Thread ourThread = null;
        SurfaceHolder ourHolder;
        volatile boolean playingSnake;
        Paint paint;

        public SnakeView(Context context) {
            super(context);
            ourHolder = getHolder();
            paint = new Paint();

            snakeX = new int[200];
            snakeY = new int[200];

            getSnake();
            getApple();
        }

        @RequiresApi(api = Build.VERSION_CODES.O)
        @Override
        public void run() {
            while (playingSnake) {
                updateGame();
                drawGame();
                controlFPS();
            }
        }

        public void getSnake() {
            snakeLength = 3;

            snakeX[0] = numBlocksWide / 2;
            snakeY[0] = numBlocksHigh / 2;

            snakeX[1] = snakeX[0] - 1;
            snakeY[1] = snakeY[0];

            snakeX[1] = snakeX[1] - 1;
            snakeY[1] = snakeY[0];
        }

        public void getApple() {
            Random random = new Random();
            appleX = random.nextInt(numBlocksWide) + 1;
            appleY = random.nextInt(numBlocksHigh - bottomGap) + 1;
        }

        public void updateGame() {
            if (snakeX[0] == appleX && snakeY[0] == appleY) {
                snakeLength++;
                getApple();
                score = score + snakeLength;

                //MANGIASOUND
                MediaPlayer mediaPlayer = MediaPlayer.create(getApplicationContext(), R.raw.snakeeat);
                mediaPlayer.start();

                MediaStopped(mediaPlayer);


            }

            for (int i = snakeLength; i > 0; i--) {
                snakeX[i] = snakeX[i - 1];
                snakeY[i] = snakeY[i - 1];
            }

            switch (directionOfTravel) {
                case 0://up
                    snakeY[0]--;
                    break;
                case 1://right
                    snakeX[0]++;
                    break;
                case 2://down
                    snakeY[0]++;
                    break;
                case 3://left
                    snakeX[0]--;
                    break;
            }

            boolean dead = false;

            if (snakeX[0] == -1) dead = true;
            if (snakeX[0] >= numBlocksWide) dead = true;
            if (snakeY[0] == 0) dead = true;
            if (snakeY[0] == numBlocksHigh) dead = true;

            for (int i = snakeLength - 1; i > 0; i--) {
                if ((i > 4) && (snakeX[0] == snakeX[i])
                        && (snakeY[0] == snakeY[i])) {
                    dead = true;
                }
            }
            if (dead) {
                //CRASH SOUND
                MediaPlayer mediaPlayer = MediaPlayer.create(getApplicationContext(), R.raw.crash);
                mediaPlayer.start();
                MediaStopped(mediaPlayer);

                Intent ScoreActivity = new Intent(getContext(), GameScoreActivity.class);
                ScoreActivity.putExtra("Score", score);
                ScoreActivity.putExtra("Game", R.string.Snake_Game_Name);
                startActivity(ScoreActivity);
                finish();
            }
        }

        @RequiresApi(api = Build.VERSION_CODES.O)
        public void drawGame() {
            if (ourHolder.getSurface().isValid()) {
                canvas = ourHolder.lockCanvas();
                Bitmap bitmap = BitmapFactory.decodeResource(getContext().getResources(), (R.drawable.background_drawer));

                canvas.drawBitmap(bitmap, null, new RectF(0, 0, screenWidth, screenHeight), paint);//the background

                paint.setColor(Color.argb(255, 3, 153, 1));

                canvas.drawRect(new RectF(lateralGap, topGap + 10, screenWidth - lateralGap, screenHeight - 50), paint);
                paint.setColor(Color.argb(255, 255, 235, 59));
                paint.setTypeface(getResources().getFont(R.font.fortnite));
                paint.setTextSize(100);

                //paint.setTypeface(getResources().getFont(R.font.quantum));
                canvas.drawText("Score:" + score,
                        30, topGap - 6, paint);
                //draw a border - 4 lines, top right, bottom , left
                paint.setStrokeWidth(3);//3 pixel border
                //Top Line
                canvas.drawLine(lateralGap, topGap + 10, screenWidth - lateralGap, topGap + 10, paint);

                //Right Line
                canvas.drawLine(screenWidth - lateralGap, topGap + 10, screenWidth -
                        lateralGap, topGap + (numBlocksHigh * blockSize), paint);

                //Bottom Line
                canvas.drawLine(screenWidth - lateralGap, topGap + (numBlocksHigh * blockSize), lateralGap, topGap +
                        (numBlocksHigh * blockSize), paint);
                //Left Line
                canvas.drawLine(lateralGap, topGap + 10, lateralGap, topGap + (numBlocksHigh * blockSize), paint);

                canvas.drawBitmap(headBitmap, snakeX[0] * blockSize,
                        (snakeY[0] * blockSize) + topGap, paint);

                for (int i = 1; i < snakeLength - 1; i++) {
                    canvas.drawBitmap(bodyBitmap,
                            snakeX[i] * blockSize,
                            (snakeY[i] * blockSize) + topGap, paint);
                }

                canvas.drawBitmap(tailBitmap, snakeX[snakeLength - 1] * blockSize,
                        (snakeY[snakeLength - 1] * blockSize) + topGap, paint);

                canvas.drawBitmap(appleBitmap, appleX * blockSize,
                        (appleY * blockSize) + topGap, paint);
                ourHolder.unlockCanvasAndPost(canvas);
            }
        }

        public void controlFPS() {
            long timeThisFrame = (System.currentTimeMillis() - lastFrameTime);
            long timeToSleep = 100 - timeThisFrame;
            if (timeThisFrame > 0) {
                fps = (int) (1000 / timeThisFrame);
            }
            if (timeToSleep > 0) {
                try {
                    Thread.sleep(timeToSleep);
                } catch (InterruptedException e) {
                }
            }
            lastFrameTime = System.currentTimeMillis();
        }

        public void pause() {
            playingSnake = false;
            try {
                ourThread.join();
            } catch (InterruptedException e) {
            }
        }

        public void resume() {
            playingSnake = true;
            ourThread = new Thread(this);
            ourThread.start();
        }

        @Override
        public boolean onTouchEvent(MotionEvent motionEvent) {
            switch (motionEvent.getAction() & MotionEvent.ACTION_MASK) {
                case MotionEvent.ACTION_DOWN:
                    startX = motionEvent.getX();
                    startY = motionEvent.getY();
                case MotionEvent.ACTION_UP:
                    endX = motionEvent.getX();
                    endY = motionEvent.getY();
            }
            SwipeDirectionDetect(startX, startY, endX, endY);

            return true;
        }


        protected void SwipeDirectionDetect(float StartX, float StartY, float EndX, float EndY) {
            float absSwypeX = Math.abs(StartX - EndX);
            float absSwypeY = Math.abs(StartY - EndY);


            if (absSwypeX != 0 && absSwypeY != 0) {
                if (absSwypeX > absSwypeY) {
                    //Swipe left/Right
                    if (StartX > EndX) {
                        //Swipe Left
                        if (directionOfTravel != 1) {
                            Log.d("Swipe", "Left");
                            directionOfTravel = 3;
                        }
                    } else {
                        //Swipe Right
                        if (directionOfTravel != 3) {
                            Log.d("Swipe", "Right");
                            directionOfTravel = 1;
                        }
                    }
                } else {
                    //Swipe Top/Down
                    if (StartY > EndY) {
                        //Swipe Up
                        if (directionOfTravel != 2) {
                            Log.d("Swipe", "Up");
                            directionOfTravel = 0;
                        }
                    } else {
                        if (directionOfTravel != 0) {
                            //Swipe Down
                            Log.d("Swipe", "Down");

                            directionOfTravel = 2;
                        }

                    }
                }
            }
        }
    }


    @Override
    protected void onResume() {
        super.onResume();
        snakeView.resume();
    }

    @Override
    protected void onPause() {
        music.stop();
        super.onPause();
        snakeView.pause();
    }


    public void configureDisplay() {
        Display display =
                getWindowManager().getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        screenWidth = size.x;
        screenHeight = size.y;
        topGap = screenHeight / 8;
        lateralGap = 20;
        bottomGap = 25;
        blockSize = screenWidth / 80;

        numBlocksWide = (screenWidth - lateralGap) / blockSize;
        numBlocksHigh = (screenHeight - topGap - bottomGap) / blockSize;

        headBitmap = BitmapFactory.decodeResource(getResources(),
                R.drawable.head_red);
        bodyBitmap = BitmapFactory.decodeResource(getResources(),
                R.drawable.head_green);
        tailBitmap = BitmapFactory.decodeResource(getResources(),
                R.drawable.head_green);
        appleBitmap = BitmapFactory.decodeResource(getResources(),
                R.drawable.fruit);

        headBitmap = Bitmap.createScaledBitmap(headBitmap,
                blockSize, blockSize, false);
        bodyBitmap = Bitmap.createScaledBitmap(bodyBitmap,
                blockSize, blockSize, false);
        tailBitmap = Bitmap.createScaledBitmap(tailBitmap,
                blockSize, blockSize, false);
        appleBitmap = Bitmap.createScaledBitmap(appleBitmap,
                blockSize, blockSize, false);

    }

    @Override
    public void onBackPressed() {
        music.stop();
        super.onBackPressed();
        finish();
    }

    private void MediaStopped(MediaPlayer mediaPlayer) {

        mediaPlayer.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
            @Override
            public void onCompletion(MediaPlayer mp) {
                mp.release();
            }
        });


    }
}
