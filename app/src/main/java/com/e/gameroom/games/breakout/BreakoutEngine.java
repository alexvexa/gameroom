package com.e.gameroom.games.breakout;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.RectF;
import android.media.MediaPlayer;
import android.os.Build;
import android.util.Log;
import android.view.MotionEvent;
import android.view.SurfaceHolder;
import android.view.SurfaceView;

import com.e.gameroom.R;
import com.e.gameroom.games.GameScoreActivity;

import androidx.annotation.RequiresApi;

import static androidx.core.content.ContextCompat.startActivity;

class BreakoutEngine extends SurfaceView implements Runnable {

    // This is our thread
    private Thread gameThread = null;

    // This is new. We need a SurfaceHolder
    // When we use Paint and Canvas in a thread
    // We will see it in action in the draw method soon.
    private SurfaceHolder ourHolder;

    // A boolean which we will set and unset
    // when the game is running- or not.
    private volatile boolean playing;

    // Game is paused at the start
    private boolean paused = true;

    // A Canvas and a Paint object
    private Canvas canvas;
    private Paint paint;

    // How wide and high is the screen?
    private int screenX;
    private int screenY;

    // This variable tracks the game frame rate
    private long fps;

    // This is used to help calculate the fps
    private long timeThisFrame;

    // The player's bat
    Bat bat;

    // A ball
    Ball ball;

    // Up to 200 bricks
    Brick[] bricks = new Brick[200];
    int numBricks = 0;

    int HittedBricks = 0;
    // The score
    int score;

    // Lives
    int lives;

    // The constructor is called when the object is first created
    public BreakoutEngine(Context context, int x, int y, int actual_score, int actual_lives) {
        // This calls the default constructor to setup the rest of the object
        super(context);

        score = actual_score;
        lives = actual_lives;

        // Initialize ourHolder and paint objects
        ourHolder = getHolder();
        paint = new Paint();

        // Initialize screenX and screenY because x and y are local
        screenX = x;
        screenY = y;

        // Initialize the player's bat
        bat = new Bat(screenX, screenY);

        // Create a ball
        ball = new Ball();

        restart();
    }

    // Runs when the OS calls onPause on BreakoutActivity method
    public void pause() {
        playing = false;
        try {
            gameThread.join();
        } catch (InterruptedException e) {
            Log.e("Error:", "joining thread");
        }
    }

    // Runs when the OS calls onResume on BreakoutActivity method
    public void resume() {
        playing = true;
        gameThread = new Thread(this);
        gameThread.start();
    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    @Override
    public void run() {
        while (playing) {

            // Capture the current time in milliseconds in startFrameTime
            long startFrameTime = System.currentTimeMillis();

            // Update the frame
            // Update the frame
            if (!paused) {
                update();
            }

            // Draw the frame
            draw();

            // Calculate the fps this frame
            // We can then use the result to
            // time animations and more.
            timeThisFrame = System.currentTimeMillis() - startFrameTime;
            if (timeThisFrame >= 1) {
                fps = 1000 / timeThisFrame;
            }

        }
    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    private void update() {
        // Move the bat if required
        bat.update(fps);

        // Update the ball
        ball.update(fps);

        // Check for ball colliding with a brick
        for (int i = 0; i < numBricks; i++) {

            if (bricks[i].getVisibility()) {

                if (RectF.intersects(bricks[i].getRect(), ball.getRect())) {
                    bricks[i].setInvisible();
                    ball.reverseYVelocity();
                    HittedBricks++;
                    score = score + 10;

                    MediaPlayer mediaPlayer = MediaPlayer.create(getContext(), R.raw.boom);
                    mediaPlayer.start();
                    MediaStopped(mediaPlayer);

                }
            }
        }

        // Check for ball colliding with bat
        if (RectF.intersects(bat.getRect(), ball.getRect())) {
            ball.setRandomXVelocity();
            ball.reverseYVelocity();
            ball.clearObstacleY(bat.getRect().top - 2);

            MediaPlayer mediaPlayer = MediaPlayer.create(getContext(), R.raw.beep);
            mediaPlayer.start();
            MediaStopped(mediaPlayer);

        }

        // Bounce the ball back when it hits the bottom of screen
        // And deduct a life
        if (ball.getRect().bottom > screenY) {
            ball.reverseYVelocity();
            ball.clearObstacleY(screenY - 2);

            // Lose a life
            lives--;


            MediaPlayer mediaPlayer = MediaPlayer.create(getContext(), R.raw.slap);
            mediaPlayer.start();
            MediaStopped(mediaPlayer);

            if (lives == 0) {
                paused = true;

                Intent FinalScoreView = new Intent(getContext(), GameScoreActivity.class);
                FinalScoreView.putExtra("Score", score);
                FinalScoreView.putExtra("Game", R.string.Breakout_Game_Name);
                startActivity(getContext(), FinalScoreView, null);
                ((Activity) getContext()).finish();

            }

        }

        // Bounce the ball back when it hits the top of screen
        if (ball.getRect().top < 0) {
            ball.reverseYVelocity();
            ball.clearObstacleY(12);

            MediaPlayer mediaPlayer = MediaPlayer.create(getContext(), R.raw.beep);
            mediaPlayer.start();
            MediaStopped(mediaPlayer);
        }

        // If the ball hits left wall bounce
        if (ball.getRect().left < 0) {
            ball.reverseXVelocity();
            ball.clearObstacleX(2);

            MediaPlayer mediaPlayer = MediaPlayer.create(getContext(), R.raw.beep);
            mediaPlayer.start();
            MediaStopped(mediaPlayer);
        }

        // If the ball hits right wall bounce
        if (ball.getRect().right > screenX - 10) {
            ball.reverseXVelocity();
            ball.clearObstacleX(screenX - 22);

            MediaPlayer mediaPlayer = MediaPlayer.create(getContext(), R.raw.beep);
            mediaPlayer.start();
            MediaStopped(mediaPlayer);
        }

        // Pause if cleared screen
        if (HittedBricks == numBricks) {
// Initialize ourHolder and paint objects
            Intent restart = new Intent(getContext(), BreakoutActivity.class);
            restart.putExtra("score", score);
            restart.putExtra("lives", lives);
            HittedBricks = 0;
            getContext().startActivity(restart);
            ((Activity) getContext()).finish();
        }
    }


    void restart() {
        // Put the ball back to the start
        ball.reset(screenX, screenY);

        int brickWidth = screenX / 10;
        int brickHeight = screenY / 12;

        // Build a wall of bricks
        numBricks = 0;

        for (int column = 1; column < 9; column++) {
            for (int row = 2; row < 7; row++) {
                bricks[numBricks] = new Brick(row, column, brickWidth, brickHeight);
                numBricks++;
            }
        }

        // Reset scores and lives
    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    private void draw() {
        // Make sure our drawing surface is valid or game will crash
        if (ourHolder.getSurface().isValid()) {
            // Lock the canvas ready to draw
            canvas = ourHolder.lockCanvas();

            // Draw the background color
            Bitmap bitmap = BitmapFactory.decodeResource(getContext().getResources(), (R.drawable.breakout_blue_background));
            canvas.drawBitmap(bitmap, null, new RectF(0, 0, screenX, screenY), paint);//the background

            // Draw everything to the screen

            // Choose the brush color for drawing
            paint.setColor(Color.argb(255, 255, 255, 255));

            // Draw the bat
            canvas.drawRoundRect(bat.getRect(), 10, 10, paint);

            // Draw the ball
            canvas.drawRoundRect(ball.getRect(), 15, 15, paint);

            // Change the brush color for drawing
            paint.setColor(Color.argb(255  , 249, 129, 0));

            // Draw the bricks if visible
            for (int i = 0; i < numBricks; i++) {
                if (bricks[i].getVisibility()) {
                    canvas.drawRect(bricks[i].getRect(), paint);
                }
            }

            // Draw the HUD
            // Choose the brush color for drawing
            paint.setColor(Color.argb(255, 255, 235, 59));

            // Draw the score
            paint.setTextSize(100);
            paint.setTypeface(getResources().getFont(R.font.fortnite));
            canvas.drawText("  Score: " + score + "   Lives: " + lives, 30, 120, paint);

            // Show everything we have drawn
            ourHolder.unlockCanvasAndPost(canvas);
        }
    }

    // The SurfaceView class implements onTouchListener
    // So we can override this method and detect screen touches.
    @Override
    public boolean onTouchEvent(MotionEvent motionEvent) {
        // Our code here
        switch (motionEvent.getAction() & MotionEvent.ACTION_MASK) {

            // Player has touched the screen
            case MotionEvent.ACTION_DOWN:

                paused = false;

                if (motionEvent.getX() > screenX / 2) {
                    bat.setMovementState(bat.RIGHT);
                } else {
                    bat.setMovementState(bat.LEFT);
                }

                break;

            // Player has removed finger from screen
            case MotionEvent.ACTION_UP:
                bat.setMovementState(bat.STOPPED);
                break;
        }

        return true;
    }


    private void MediaStopped(MediaPlayer mediaPlayer) {

        mediaPlayer.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
            @Override
            public void onCompletion(MediaPlayer mp) {
                mp.release();
            }
        });


    }
}
