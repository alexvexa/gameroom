package com.e.gameroom.games.breakout;

import android.app.Activity;
import android.graphics.Point;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.view.Display;

import com.e.gameroom.R;

public class BreakoutActivity extends Activity {

    BreakoutEngine breakoutEngine;
    MediaPlayer music;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        int score = getIntent().getIntExtra("score", 0);
        int lives = getIntent().getIntExtra("lives", 3);
        music = MediaPlayer.create(this, R.raw.breakout_song);
        music.setLooping(true);
        music.start();

        // Get a Display object to access screen details
        Display display = getWindowManager().getDefaultDisplay();
        // Load the resolution into a Point object
        Point size = new Point();
        display.getSize(size);

        // Initialize gameView and set it as the view
        breakoutEngine = new BreakoutEngine(this, size.x, size.y, score, lives);
        setContentView(breakoutEngine);
    }

    // This method executes when the player starts the game
    @Override
    protected void onResume() {
        super.onResume();

        // Tell the gameView resume method to execute
        breakoutEngine.resume();
    }

    // This method executes when the player quits the game
    @Override
    protected void onPause() {
        super.onPause();
        music.stop();

        // Tell the gameView pause method to execute
        breakoutEngine.pause();
    }


    @Override
    public void onBackPressed() {
        music.stop();
        super.onBackPressed();
        finish();
    }
}
