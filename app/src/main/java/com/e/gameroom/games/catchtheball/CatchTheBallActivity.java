package com.e.gameroom.games.catchtheball;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Point;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.os.Handler;
import android.view.Display;
import android.view.MotionEvent;
import android.view.View;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import com.e.gameroom.database.DatabaseHelper;
import com.e.gameroom.R;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

import java.util.Timer;
import java.util.TimerTask;

import androidx.appcompat.app.AppCompatActivity;

public class CatchTheBallActivity extends AppCompatActivity {
    public static final int GAME_ID = 1;
    public static final String TAG_LOG = CatchTheBallActivity.class.getName();
    DatabaseHelper myDb;
    static final int START_POSITION = -80;

    TextView StartLabel, ScoreLabel, FinalScore, GameOverLabel, TryAgain, GoBack;
    ImageView mMonster, mOrangeFruit, mRedFruit, mBomb;

    //Monster y-axis
    private int mMonsterY;

    //Orange Fruit y-axis and x-axis
    private int mOrangeFruitX;
    private int mOrangeFruitY;


    //Red Fruit y-axis and x-axis
    private int mRedFruitX;
    private int mRedFruitY;


    //Bomb Fruit y-axis and x-axis
    private int mBombX;
    private int mBombY;

    //Altezza frame_Layout del gioco
    private int frameHeight;

    //Altezza del Mostro
    private int mMonsterSize;

    //Dimensioni Schermo
    private int screenWidth;
    private int screenHeight;
    private int score = 0;

    private int RedHitScore = 50;
    private int OrangeHitScore = 10;
    MediaPlayer music;

    //Initlialize class
    private Handler handler = new Handler();
    private Timer timer = new Timer();

    //Status Check
    private boolean actionFlag = false;
    private boolean StartedGame = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_catch_the_ball_);

        music = MediaPlayer.create(this, R.raw.catch_the_ball_song);
        music.setLooping(true);
        music.start();


        //Connect to Database
        myDb = new DatabaseHelper(this);

        ScoreLabel = findViewById(R.id.ScoreLabel);
        StartLabel = findViewById(R.id.TapLabel);
        mMonster = findViewById(R.id.Monster);
        mBomb = findViewById(R.id.black_Bomb);
        mOrangeFruit = findViewById(R.id.orange_Fruit);
        mRedFruit = findViewById(R.id.red_Fruit);
        FinalScore = findViewById(R.id.FinalScore);
        GameOverLabel = findViewById(R.id.gameover);
        GoBack = findViewById(R.id.GoBack);
        TryAgain = findViewById(R.id.TryAgain);

        TryAgain.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent restartGame = new Intent(getApplicationContext(), CatchTheBallActivity.class);
                startActivity(restartGame);
                finish();
            }
        });

        GoBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });


        //Get Screen Size
        WindowManager wm = getWindowManager();
        Display screen = wm.getDefaultDisplay();
        Point size = new Point();
        screen.getSize(size);
        screenWidth = size.x;
        screenHeight = size.y;


        setItemsOutOfScreen();

    }

    public void changePos() {

        hitCheck();
        //Movimenti mostro
        monsterMovement();

        //Movimenti OrangeFruit
        orangeFruitMovement();

        //Movimenti Bomba
        bombMovement();

        //Movimenti RedFruit
        RedFruitMovement();
    }

    private void hitCheck() {
        //If the center of the ball is in the box, it counts as a hit.

        //OrangeFruitCenter
        int orangeFruitCenterX = mOrangeFruitX + mOrangeFruit.getWidth() / 2;
        int orangeFruitCenterY = mOrangeFruitY + mOrangeFruit.getHeight() / 2;

        //RedFruitCenter
        int redFruitCenterX = mRedFruitX + mRedFruit.getWidth() / 2;
        int redFruitCenterY = mRedFruitY + mRedFruit.getHeight() / 2;

        //BombCenter
        int bombCenterX = mBombX + mBomb.getWidth() / 2;
        int bombCenterY = mBombY + mBomb.getHeight() / 2;

        //Bomb Fruit hit the monster
        if (0 <= bombCenterX && bombCenterX <= mMonsterSize && mMonsterY <= bombCenterY && bombCenterY < mMonsterY + mMonsterSize) {


            //Stop timer
            timer.cancel();
            timer = null;

            MediaPlayer mediaPlayer = MediaPlayer.create(getApplicationContext(), R.raw.hit);
            mediaPlayer.start();
            MediaStopped(mediaPlayer);

            //Show Result
            FinalScore.setText(FinalScore.getText() + String.valueOf(score));
            FinalScore.setVisibility(View.VISIBLE);
            GameOverLabel.setVisibility(View.VISIBLE);
            TryAgain.setVisibility(View.VISIBLE);
            GoBack.setVisibility(View.VISIBLE);

            saveScoreOnDatabase();


        }

        //Red Fruit hit the monster
        if (0 <= redFruitCenterX && redFruitCenterX <= mMonsterSize && mMonsterY <= redFruitCenterY && redFruitCenterY < mMonsterY + mMonsterSize) {

            MediaPlayer mediaPlayer = MediaPlayer.create(getApplicationContext(), R.raw.crunch);
            mediaPlayer.start();
            MediaStopped(mediaPlayer);

            score += RedHitScore;
            ScoreLabel.setText("Score: " + score);
            mRedFruitX = -10;
        }


        //Orange Fruit hit the monster
        if (0 <= orangeFruitCenterX && orangeFruitCenterX <= mMonsterSize && mMonsterY <= orangeFruitCenterY && orangeFruitCenterY < mMonsterY + mMonsterSize) {

            MediaPlayer mediaPlayer = MediaPlayer.create(getApplicationContext(), R.raw.crunch);
            mediaPlayer.start();
            MediaStopped(mediaPlayer);

            score += OrangeHitScore;
            ScoreLabel.setText("Score: " + score);
            mOrangeFruitX = -10;
        }


    }


    public boolean onTouchEvent(MotionEvent me) {
        //Touch in caso di game non iniziato

        if (!StartedGame) {
            StartedGame = true;
            StartLabel.setVisibility(View.GONE);

            FrameLayout frame = findViewById(R.id.frame);
            frameHeight = frame.getHeight();

            mMonsterY = (int) mMonster.getY();

            mMonsterSize = mMonster.getHeight();

            timer.schedule(new TimerTask() {
                @Override
                public void run() {
                    handler.post(new Runnable() {
                        @Override
                        public void run() {
                            changePos();
                        }
                    });
                }
            }, 0, 20);
        } else {
            if (me.getAction() == MotionEvent.ACTION_DOWN) {
                actionFlag = true;
            } else if (me.getAction() == MotionEvent.ACTION_UP) {
                actionFlag = false;
            }
        }

        return true;
    }

    private void setItemsOutOfScreen() {
        mOrangeFruit.setX(START_POSITION);
        mOrangeFruit.setY(START_POSITION);
        mBomb.setX(START_POSITION);
        mBomb.setY(START_POSITION);
        mRedFruit.setX(START_POSITION);
        mRedFruit.setY(START_POSITION);
    }

    //Funzione che determina i movimenti del mostro
    private void monsterMovement() {
        if (actionFlag) {
            //If Touching go Up
            mMonsterY -= 20;
        } else {
            //If not Touching go down

            mMonsterY += 20;
        }

        //Altezza massima Monster
        if (mMonsterY < 0) {
            mMonsterY = 0;
        }

        //Altezza minima Monster
        if (mMonsterY > frameHeight - mMonsterSize) {
            mMonsterY = frameHeight - mMonsterSize;
        }

        //Imposta la nuova cordinata Y del mostro
        mMonster.setY(mMonsterY);
    }

    private void orangeFruitMovement() {
        mOrangeFruitX -= (12 + score / 75);

        if (mOrangeFruitX < 0) {
            mOrangeFruitX = screenWidth + 50;
            mOrangeFruitY = (int) Math.floor(Math.random() * (frameHeight - mOrangeFruit.getHeight()));
        }
        mOrangeFruit.setX(mOrangeFruitX);
        mOrangeFruit.setY(mOrangeFruitY);
    }

    ;


    private void RedFruitMovement() {
        mRedFruitX -= (12 + score / 50);

        if (mRedFruitX < 0) {
            mRedFruitX = screenWidth + 5000;
            mRedFruitY = (int) Math.floor(Math.random() * (frameHeight - mRedFruit.getHeight()));
        }
        mRedFruit.setX(mRedFruitX);
        mRedFruit.setY(mRedFruitY);
    }

    ;

    private void bombMovement() {
        mBombX -= (16 + score / 75);

        if (mBombX < 0) {
            mBombX = screenWidth + 10;
            mBombY = (int) Math.floor(Math.random() * (frameHeight - mBomb.getHeight()));
        }
        mBomb.setX(mBombX);
        mBomb.setY(mBombY);
    }

    ;

    @Override
    public void onBackPressed() {
        finish();
        music.stop();

    }


    @Override
    protected void onPause() {
        super.onPause();
        music.stop();
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    private void saveScoreOnDatabase() {
        FirebaseUser currentUser = FirebaseAuth.getInstance().getCurrentUser();
        if (currentUser == null) {
            final EditText Username = new EditText(getApplicationContext());
            final AlertDialog.Builder InsertGuestGame = new AlertDialog.Builder(this);
            InsertGuestGame.setTitle("Inserisci il tuo Username:");
            InsertGuestGame.setView(Username);

            InsertGuestGame.setPositiveButton(R.string.Confirmation, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    String InsertedUsername = Username.getText().toString();
                    if (InsertedUsername.isEmpty()) {
                        saveScoreOnDatabase();
                    } else {
                        myDb.updateScore(getResources().getString(R.string.catch_the_ball_Name), score, InsertedUsername);
                    }
                }
            });

            InsertGuestGame.show();
        } else {
            myDb.updateScore(getResources().getString(R.string.catch_the_ball_Name), score, null);
            music.stop();

        }


    }

    private void MediaStopped(MediaPlayer mediaPlayer) {

        mediaPlayer.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
            @Override
            public void onCompletion(MediaPlayer mp) {
                mp.release();
            }
        });


    }
}
