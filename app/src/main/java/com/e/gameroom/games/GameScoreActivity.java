package com.e.gameroom.games;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import com.e.gameroom.database.DatabaseHelper;
import com.e.gameroom.R;
import com.e.gameroom.games.breakout.BreakoutActivity;
import com.e.gameroom.games.snake.SnakeActivity;
import com.e.gameroom.games.spaceinv.SpaceInvadersActivity;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

import androidx.appcompat.app.AppCompatActivity;


public class GameScoreActivity extends AppCompatActivity {
    public static final String TAG_LOG = GameScoreActivity.class.getName();

    private int score;
    private int mGameId;
    private DatabaseHelper mDatabaseHelper;

    //Interface Elements
    private TextView mFinalScore, mTryAgainLink, mGoBackLink;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.game_score_layout);
        mFinalScore = findViewById(R.id.FinalScore);
        mTryAgainLink = findViewById(R.id.TryAgain);
        mGoBackLink = findViewById(R.id.GoBack);

        //Connect to Database
        mDatabaseHelper = new DatabaseHelper(this);

        //Personalize Interface and realized Score
        setBackground();
        setScore();

        //Save Score on Database (Firebase or SQLite)
        saveScoreOnDatabase();

        //Try Again Link Action on Click
        mTryAgainLink.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent restartGame;

                //Execute GameActivity selected from received gameId
                switch (mGameId) {
                    case R.string.Breakout_Game_Name:
                        restartGame = new Intent(getApplicationContext(), BreakoutActivity.class);
                        startActivity(restartGame);
                        finish();
                        break;
                    case R.string.Snake_Game_Name:
                        restartGame = new Intent(getApplicationContext(), SnakeActivity.class);
                        startActivity(restartGame);
                        finish();
                        break;
                    case R.string.SpaceInvaders_Game_Name:
                        restartGame = new Intent(getApplicationContext(), SpaceInvadersActivity.class);
                        startActivity(restartGame);
                        finish();
                        break;
                }
            }
        });

        //Go Back Link Action on Click
        mGoBackLink.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });


    }

    //Set Personalizated Game Background from received gameId
    private void setBackground() {
        mGameId = getIntent().getIntExtra("Game", 0);
        switch (mGameId) {
            case R.string.Breakout_Game_Name:
                Log.d(TAG_LOG, "Breakout Layout");
                getWindow().setBackgroundDrawable(getResources().getDrawable(R.drawable.breakout_blue_background));
                break;

            case R.string.Snake_Game_Name:
                Log.d(TAG_LOG, "Snake Layout");
                getWindow().setBackgroundDrawable(getResources().getDrawable(R.drawable.snake_background));
                break;

            case R.string.SpaceInvaders_Game_Name:
                Log.d(TAG_LOG, "Space Invaders Layout");
                getWindow().setBackgroundDrawable(getResources().getDrawable(R.drawable.spaceinvaders_background));
                break;
        }

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }

    //Set Score on FinalScore
    protected void setScore() {
        score = getIntent().getIntExtra("Score", 0);
        mFinalScore.setText(mFinalScore.getText() + String.valueOf(score));
    }

    //Save Score on Database (Firebase or SQLite)
    private void saveScoreOnDatabase() {
        //Firebase current User instantiation
        FirebaseUser currentUser = FirebaseAuth.getInstance().getCurrentUser();

        if (currentUser == null) {
            //Save on Local Database

            //AlertDialog Build for insert personalized Username before saving score
            final EditText username = new EditText(getApplicationContext());
            final AlertDialog.Builder putUsernameDialog = new AlertDialog.Builder(this);
            putUsernameDialog.setTitle(getResources().getString(R.string.InsertUsername));
            putUsernameDialog.setView(username);

            //Confirmation Action on Click
            putUsernameDialog.setPositiveButton(R.string.Confirmation, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    String InsertedUsername = username.getText().toString();
                    if (InsertedUsername.isEmpty()) {
                        //If Username is Empty recreate AlertDialog
                        saveScoreOnDatabase();
                    } else {

                        //Save score with DatabaseHelper Method
                        mDatabaseHelper.updateScore(getResources().getString(mGameId), score, InsertedUsername);
                    }
                }
            });

            // AlertDialog Show
            putUsernameDialog.show();
        } else {
            //Logged User saving score with DatabaseHelper Method
            mDatabaseHelper.updateScore(getResources().getString(mGameId), score, null);

        }
    }
}
