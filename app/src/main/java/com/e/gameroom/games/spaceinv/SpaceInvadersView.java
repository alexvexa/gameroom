package com.e.gameroom.games.spaceinv;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.RectF;
import android.media.MediaPlayer;
import android.os.Build;
import android.util.Log;
import android.view.MotionEvent;
import android.view.SurfaceHolder;
import android.view.SurfaceView;

import com.e.gameroom.R;
import com.e.gameroom.games.GameScoreActivity;

import androidx.annotation.RequiresApi;

import static androidx.core.content.ContextCompat.startActivity;

public class SpaceInvadersView extends SurfaceView implements Runnable {

    private Context context;


    // This is our thread
    private Thread gameThread = null;

    // Our SurfaceHolder to lock the surface before we draw our graphics
    private SurfaceHolder ourHolder;

    // A boolean which we will set and unset
    // when the game is running- or not.
    private volatile boolean playing;

    // Game is paused at the start
    private boolean paused = true;

    // A Canvas and a Paint object
    private Canvas canvas;
    private Paint paint;

    // This variable tracks the game frame rate
    private long fps;

    // This is used to help calculate the fps
    private long timeThisFrame;

    // The size of the screen in pixels
    private int screenX;
    private int screenY;

    // The players ship
    private PlayerShip playerShip;

    // The player's bullet
    private Bullet bullet;

    // The invaders bullets
    private Bullet[] invadersBullets = new Bullet[200];
    private int nextBullet;
    private int maxInvaderBullets = 10;

    // Up to 60 invaders
    private Invader[] invaders = new Invader[60];
    private int numInvaders = 0;
    private int hittedInvaders = 0;

    // The player's shelters are built from bricks
    private DefenceBrick[] bricks = new DefenceBrick[400];
    private int numBricks;


    // The score
    private int score = 0;

    // Lives
    private int lives = 3;

    // How menacing should the sound be?
    private long menaceInterval = 1000;
    // Which menace sound should play next
    private boolean uhOrOh;
    // When did we last play a menacing sound
    private long lastMenaceTime = System.currentTimeMillis();

    // When the we initialize (call new()) on gameView
    // This special constructor method runs
    public SpaceInvadersView(Context context, int x, int y) {

        // The next line of code asks the
        // SurfaceView class to set up our object.
        // How kind.
        super(context);

        // Make a globally available copy of the context so we can use it in another method
        this.context = context;

        // Initialize ourHolder and paint objects
        ourHolder = getHolder();
        paint = new Paint();

        screenX = x;
        screenY = y;


        prepareLevel();
    }

    private void prepareLevel() {

        // Here we will initialize all the game objects

        // Make a new player space ship
        playerShip = new PlayerShip(context, screenX, screenY);

        // Prepare the players bullet
        bullet = new Bullet(screenY);

        // Initialize the invadersBullets array
        for (int i = 0; i < invadersBullets.length; i++) {
            invadersBullets[i] = new Bullet(screenY);
        }

        // Build an army of invaders
        numInvaders = 0;
        for (int column = 0; column < 6; column++) {
            for (int row = 2; row < 5; row++) {
                invaders[numInvaders] = new Invader(context, row, column, screenX, screenY);
                numInvaders++;
            }
        }

        // Build the shelters
        numBricks = 0;
        for (int shelterNumber = 0; shelterNumber < 4; shelterNumber++) {
            for (int column = 0; column < 10; column++) {
                for (int row = 0; row < 5; row++) {
                    bricks[numBricks] = new DefenceBrick(row, column, shelterNumber, screenX, screenY);
                    numBricks++;
                }
            }
        }


        // Reset the menace level
        menaceInterval = 1000;

    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    @Override
    public void run() {

        while (playing) {

            // Capture the current time in milliseconds in startFrameTime
            long startFrameTime = System.currentTimeMillis();

            // Update the frame
            if (!paused) {
                update();
            }

            // Draw the frame
            draw();


            // Calculate the fps this frame
            // We can then use the result to
            // time animations and more.
            timeThisFrame = System.currentTimeMillis() - startFrameTime;
            if (timeThisFrame >= 1) {
                fps = 1000 / timeThisFrame;
            }

            // We will do something new here towards the end of the project
            // Play a sound based on the menace level
            if (!paused) {
                if ((startFrameTime - lastMenaceTime) > menaceInterval) {
                    if (uhOrOh) {
                        // Play Uh
                        MediaPlayer mediaPlayer = MediaPlayer.create(context, R.raw.uh);
                        mediaPlayer.start();
                        MediaStopped(mediaPlayer);

                    } else {
                        // Play Oh
                        MediaPlayer mediaPlayer = MediaPlayer.create(context, R.raw.oh);
                        mediaPlayer.start();

                        MediaStopped(mediaPlayer);

                    }

                    // Reset the last menace time
                    lastMenaceTime = System.currentTimeMillis();
                    // Alter value of uhOrOh
                    uhOrOh = !uhOrOh;
                }
            }

        }


    }

    private void update() {

        // Did an invader bump into the side of the screen
        boolean bumped = false;

        // Has the player lost
        boolean lost = false;

        // Move the player's ship
        playerShip.update(fps);


        // Update the invaders if visible

        // Update the players bullet
        if (bullet.getStatus()) {
            bullet.update(fps);
        }

        // Update all the invaders bullets if active
        for (int i = 0; i < invadersBullets.length; i++) {
            if (invadersBullets[i].getStatus()) {
                invadersBullets[i].update(fps);
            }
        }

        // Update all the invaders if visible
        for (int i = 0; i < numInvaders; i++) {
            if (invaders[i].getVisibility()) {

                // Move the next invader
                invaders[i].update(fps);

                // Does he want to take a shot?
                if (invaders[i].takeAim(playerShip.getX(), playerShip.getLength())) {

                    // If so try and spawn a bullet
                    if (invadersBullets[nextBullet].shoot(invaders[i].getX() + invaders[i].getLength() / 2, invaders[i].getY(), bullet.DOWN)) {

                        // Shot fired
                        // Prepare for the next shot
                        nextBullet++;

                        // Loop back to the first one if we have reached the last
                        if (nextBullet == maxInvaderBullets) {
                            // This stops the firing of another bullet until one completes its journey
                            // Because if bullet 0 is still active shoot returns false.
                            nextBullet = 0;
                        }
                    }
                }

                // If that move caused them to bump the screen change bumped to true
                if (invaders[i].getX() > screenX - invaders[i].getLength()
                        || invaders[i].getX() < 0) {

                    bumped = true;


                }
            }

        }


        // Did an invader bump into the edge of the screen
        if (bumped) {
            // Move all the invaders down and change direction
            for (int i = 0; i < numInvaders; i++) {
                invaders[i].dropDownAndReverse();

                // Have the invaders landed
                if (invaders[i].getY() > screenY - screenY / 10) {
                    lost = true;
                }


            }

            // Increase the menace level
            // By making the sounds more frequent
            menaceInterval = menaceInterval - 80;


        }


        if (lost) {
            prepareLevel();
        }


        // Has the player's bullet hit the top of the screen
        if (bullet.getImpactPointY() < 0) {
            bullet.setInactive();
        }

        // Has an invaders bullet hit the bottom of the screen
        for (int i = 0; i < invadersBullets.length; i++) {

            if (invadersBullets[i].getImpactPointY() > screenY) {
                invadersBullets[i].setInactive();
            }
        }

        // Has the player's bullet hit an invader
        if (bullet.getStatus()) {
            for (int i = 0; i < numInvaders; i++) {
                if (invaders[i].getVisibility()) {
                    if (RectF.intersects(bullet.getRect(), invaders[i].getRect())) {
                        invaders[i].setInvisible();

                        MediaPlayer mediaPlayer = MediaPlayer.create(context, R.raw.invaderexplode);
                        mediaPlayer.start();

                        MediaStopped(mediaPlayer);
                        bullet.setInactive();
                        score = score + 10;
                        hittedInvaders++;

                        // Has the player won
                        if (hittedInvaders == numInvaders) {
                            hittedInvaders = 0;
                            prepareNextLevel();
                        }
                    }
                }
            }
        }

        // Has an alien bullet hit a shelter brick
        for (int i = 0; i < invadersBullets.length; i++) {
            if (invadersBullets[i].getStatus()) {
                for (int j = 0; j < numBricks; j++) {
                    if (bricks[j].getVisibility()) {
                        if (RectF.intersects(invadersBullets[i].getRect(), bricks[j].getRect())) {
                            // A collision has occurred
                            invadersBullets[i].setInactive();
                            bricks[j].setInvisible();

                            MediaPlayer mediaPlayer = MediaPlayer.create(context, R.raw.damageshelter);
                            mediaPlayer.start();
                            MediaStopped(mediaPlayer);
                        }
                    }
                }
            }

        }

        // Has a player bullet hit a shelter brick
        if (bullet.getStatus()) {
            for (int i = 0; i < numBricks; i++) {
                if (bricks[i].getVisibility()) {
                    if (RectF.intersects(bullet.getRect(), bricks[i].getRect())) {
                        // A collision has occurred
                        bullet.setInactive();
                        bricks[i].setInvisible();

                        MediaPlayer mediaPlayer = MediaPlayer.create(context, R.raw.damageshelter);
                        mediaPlayer.start();

                        MediaStopped(mediaPlayer);


                    }
                }
            }
        }

        // Has an invader bullet hit the player ship
        for (int i = 0; i < invadersBullets.length; i++) {
            if (invadersBullets[i].getStatus()) {
                if (RectF.intersects(playerShip.getRect(), invadersBullets[i].getRect())) {
                    invadersBullets[i].setInactive();
                    lives--;


                    MediaPlayer mediaPlayer = MediaPlayer.create(context, R.raw.playerexplode);
                    mediaPlayer.start();


                    // Is it game over?
                    if (lives == 0) {
                        Intent FinalScoreView = new Intent(getContext(), GameScoreActivity.class);
                        FinalScoreView.putExtra("Score", score);
                        FinalScoreView.putExtra("Game", R.string.SpaceInvaders_Game_Name);
                        startActivity(getContext(), FinalScoreView, null);
                        ((Activity) getContext()).finish();
                    }
                }
            }
        }


    }


    @RequiresApi(api = Build.VERSION_CODES.O)
    private void draw() {
        // Make sure our drawing surface is valid or we crash
        if (ourHolder.getSurface().isValid()) {
            // Lock the canvas ready to draw
            canvas = ourHolder.lockCanvas();

            // Draw the background color
            canvas.drawColor(Color.argb(255, 76, 42, 94));

            // Choose the brush color for drawing
            paint.setColor(Color.argb(255, 12, 229, 12));

            // Now draw the player spaceship
            canvas.drawBitmap(playerShip.getBitmap(), playerShip.getX(), screenY - 50, paint);


            // Draw the invaders
            for (int i = 0; i < numInvaders; i++) {
                if (invaders[i].getVisibility()) {
                    if (uhOrOh) {
                        canvas.drawBitmap(invaders[i].getBitmap(), invaders[i].getX(), invaders[i].getY(), paint);
                    } else {
                        canvas.drawBitmap(invaders[i].getBitmap2(), invaders[i].getX(), invaders[i].getY(), paint);
                    }
                }
            }


            // Draw the bricks if visible
            for (int i = 0; i < numBricks; i++) {
                if (bricks[i].getVisibility()) {
                    canvas.drawRect(bricks[i].getRect(), paint);
                }
            }


            // Draw the players bullet if active
            if (bullet.getStatus()) {
                canvas.drawRect(bullet.getRect(), paint);
            }

            // Draw the invaders bullets

            // Update all the invader's bullets if active
            for (int i = 0; i < invadersBullets.length; i++) {
                if (invadersBullets[i].getStatus()) {
                    canvas.drawRect(invadersBullets[i].getRect(), paint);
                }
            }


            // Draw the score and remaining lives
            // Change the brush color
            paint.setColor(Color.argb(255, 255, 235, 59));

            // Draw the score
            paint.setTextSize(100);
            paint.setTypeface(getResources().getFont(R.font.fortnite));
            canvas.drawText("  Score: " + score + "   Lives: " + lives, 30, 120, paint);

            // Draw everything to the screen
            ourHolder.unlockCanvasAndPost(canvas);
        }
    }

    // If SpaceInvadersActivity is paused/stopped
    // shutdown our thread.
    public void pause() {
        playing = false;
        try {
            gameThread.join();
        } catch (InterruptedException e) {
            Log.e("Error:", "joining thread");
        }

    }

    // If SpaceInvadersActivity is started then
    // start our thread.
    public void resume() {
        playing = true;
        gameThread = new Thread(this);
        gameThread.start();
    }

    // The SurfaceView class implements onTouchListener
    // So we can override this method and detect screen touches.
    @Override
    public boolean onTouchEvent(MotionEvent motionEvent) {

        switch (motionEvent.getAction() & MotionEvent.ACTION_MASK) {

            // Player has touched the screen
            case MotionEvent.ACTION_DOWN:

                paused = false;

                if (motionEvent.getY() > screenY - screenY / 2) {
                    if (motionEvent.getX() > screenX / 2) {
                        Log.d("Tag", "right");
                        if (playerShip.getX() > screenX) {
                            playerShip.setX(screenX - playerShip.getLength());
                        } else {

                            playerShip.setMovementState(playerShip.RIGHT);

                        }


                    } else {

                        Log.d("Tag", "left");


                        if (playerShip.getX() < 0) {

                            playerShip.setX(playerShip.getLength());
                        } else {

                            playerShip.setMovementState(playerShip.LEFT);

                        }

                    }


                }

                if (motionEvent.getY() < screenY - screenY / 8) {
                    // Shots fired
                    if (bullet.shoot(playerShip.getX() + playerShip.getLength() / 2, screenY, bullet.UP)) {

                        MediaPlayer mediaPlayer = MediaPlayer.create(context, R.raw.shoot);
                        mediaPlayer.start();
                        MediaStopped(mediaPlayer);
                    }
                }

                break;


            // Player has removed finger from screen
            case MotionEvent.ACTION_UP:
                playerShip.setMovementState(playerShip.STOPPED);

                Log.d("Tag", "up");
                break;
        }
        return true;
    }


    private void MediaStopped(MediaPlayer mediaPlayer) {

        mediaPlayer.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
            @Override
            public void onCompletion(MediaPlayer mp) {
                mp.release();
            }
        });


    }


    private void prepareNextLevel() {

        // Here we will initialize all the game objects

        // Initialize the invadersBullets array
        for (int i = 0; i < invadersBullets.length; i++) {
            invadersBullets[i] = new Bullet(screenY);
        }

        // Build an army of invaders
        numInvaders = 0;
        for (int column = 0; column < 6; column++) {
            for (int row = 2; row < 5; row++) {
                invaders[numInvaders] = new Invader(context, row, column, screenX, screenY);
                numInvaders++;
            }
        }

        // Reset the menace level
        menaceInterval = 1000;

    }
}
