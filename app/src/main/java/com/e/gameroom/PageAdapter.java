package com.e.gameroom;

import android.content.Context;

import androidx.annotation.Nullable;
import androidx.annotation.StringRes;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;

//Class that builds the interface of the page on which the Fragments of the individual games will then be enabled
    public class PageAdapter extends FragmentPagerAdapter {

    //Set title for Layout Tab
    @StringRes
    private static final int[] TAB_TITLES = new int[]{
            R.string.catch_the_ball_Name,
            R.string.Snake_Game_Name,
            R.string.Breakout_Game_Name,
            R.string.SpaceInvaders_Game_Name};

    private final Context mContext;

    //PageAdapter Constructor
    public PageAdapter(Context context, FragmentManager fragment) {
        super(fragment);
        mContext = context;
    }

    //Score Fragment instantiation for all games
    public Fragment getItem(int index) {
        Fragment Instance = ScoresFragment.newInstance(String.valueOf(getPageTitle(index)));
        return Instance;
    }


    @Nullable
    @Override
    public CharSequence getPageTitle(int position) {
        return mContext.getResources().getString(TAB_TITLES[position]);
    }

    @Override
    public int getCount() {
        // Show 4 total pages.
        return 4;
    }


    @Override
    public void notifyDataSetChanged() {
        super.notifyDataSetChanged();
    }

}


