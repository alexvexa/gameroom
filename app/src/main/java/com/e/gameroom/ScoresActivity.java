package com.e.gameroom;

import android.os.Bundle;
import android.os.PersistableBundle;
import android.view.View;
import android.widget.ImageView;

import com.google.android.material.tabs.TabLayout;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

import java.util.ArrayList;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.viewpager.widget.ViewPager;

public class ScoresActivity extends AppCompatActivity {

    //Firebase Connect
    private FirebaseUser mCurrentUser;

    //Interface elements
    private ImageView mBackArrow;
    private ViewPager mViewPager;

    //Adapter variables
    private ScoresAdapter mAdapter;
    private List<GameScores> mScoreList;
    private PageAdapter mPageAdapter;
    TabLayout mTabs;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_scores);

        //Elements Assignment
        mBackArrow = findViewById(R.id.BackButton);
        mViewPager = findViewById(R.id.ViewPager);
        mTabs = findViewById(R.id.tabs);

        //Method that sets the tab with the ViewPager
        mTabs.setupWithViewPager(mViewPager);

        //Firebase instantiation
        mCurrentUser = FirebaseAuth.getInstance().getCurrentUser();

        //Back Arrow Action on Click
        mBackArrow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        //Page Adapter if User is registered (Reading data from Firebase)
        if (mCurrentUser != null) {
            mPageAdapter = new PageAdapter(this, getSupportFragmentManager());
            mViewPager.setAdapter(mPageAdapter);
        }

        //Creation of the list of scores stored locally
        else {
            mScoreList = new ArrayList<>();
            mScoreList.add(new GameScores(getResources().getString(R.string.catch_the_ball_Name),getApplicationContext()));
            mScoreList.add(new GameScores(getResources().getString(R.string.Snake_Game_Name), getApplicationContext()));
            mScoreList.add(new GameScores(getResources().getString(R.string.Breakout_Game_Name),getApplicationContext()));
            mScoreList.add(new GameScores(getResources().getString(R.string.SpaceInvaders_Game_Name), getApplicationContext()));

            mAdapter = new ScoresAdapter(mScoreList, getApplicationContext());
            mViewPager.setAdapter(mAdapter);
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }

    @Override
    public void onSaveInstanceState(@NonNull Bundle outState, @NonNull PersistableBundle outPersistentState) {
        super.onSaveInstanceState(outState, outPersistentState);
    }

    @Override
    protected void onRestoreInstanceState(@NonNull Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
    }
}
