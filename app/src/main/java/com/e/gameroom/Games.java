package com.e.gameroom;

public class Games {

    private int Image;
    private String Title;

    public Games(int gameImage, String gameTtle) {
        this.Image = gameImage;
        this.Title = gameTtle;
    }

    public int getImage() {
        return Image;
    }

    public String getTitle() {
        return Title;
    }
}
